/**
 * @fileOverview The main wrapper of the application.
 */

import React from 'react';
import './App.css';
import Splash from './pages/Splash';
import New from './pages/New';
import Collection from './pages/Collection';
import Account from './pages/Account';
import NavBar from './components/NavBar';
import { Routes, Route, useLocation } from 'react-router-dom';
import Grid from '@mui/material/Grid';
import { createTheme } from '@mui/material';
import { ThemeProvider, makeStyles } from '@mui/styles';
import { UserAuthContextProvider } from './contexts/AuthContext';
import ProtectedRoute from './components/ProtectedRoute';

/**
 * Saves an object of the created theme after a function is invoked to create the Material UI Theme.
 * @type {Object}
 * @memberOf App
 */
const theme = createTheme();
const useStyles = makeStyles((theme) => ({
  splashStyle: {
    backgroundColor: '#173D5D',
    height: '100vh',
    overflowX: 'hidden'
  },
  appPageContainerStyle: {
    width: '100%',
    overflowX: 'hidden',
    overflowY: 'auto'
  },
  bottomNavigationStyle: {
    paddingTop: '55px'
  }
})
);

function ReturnLocationPath () {
  const location = useLocation();
  return location.pathname;
}

/**
 * The main wrapper of the application
 * @namespace App
 * @returns {JSX.Element}
 */
function App () {
  const classes = useStyles();
  const currentPage = ReturnLocationPath();

  return <ThemeProvider theme={theme}> {
    <div className="App">

      <UserAuthContextProvider>

          <Routes>
            <Route exact path="/" element={<Splash/>}/>
            <Route
                path="/collection"
                element={
                  <ProtectedRoute>
                    <Collection />
                  </ProtectedRoute>
                }
            />
            <Route
                path="/new"
                element={
                  <ProtectedRoute>
                    <New />
                  </ProtectedRoute>
                }
            />
            <Route
                path="/account"
                element={
                  <ProtectedRoute>
                    <Account />
                  </ProtectedRoute>
                }
            />
          </Routes>
      </UserAuthContextProvider>

      {
        currentPage !== '/'
          ? <Grid container justifyContent="center" className={classes.bottomNavigationStyle}>
              <NavBar/>
            </Grid>
          : null
      }

    </div>
  }</ThemeProvider>;
}

export default App;
