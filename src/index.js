/**
 * @fileOverview Entry point for the application.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import reportWebVitals from './reportWebVitals';

import { BrowserRouter } from 'react-router-dom';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { indigo } from '@mui/material/colors';

/**
 * Main theme of the application
 * @type {Theme}
 */
const appTheme = createTheme({
  palette: {
    mode: 'light',
    primary: {
      main: indigo[500]
    }
  }
});

/**
 * The DOM renderer
 */
ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <ThemeProvider theme={appTheme}>
                <App />
            </ThemeProvider>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
/* reportWebVitals(); */
