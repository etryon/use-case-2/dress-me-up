import { deleteObject, getStorage, ref } from 'firebase/storage';
import {
  getDoc,
  getFirestore,
  setDoc,
  updateDoc,
  deleteDoc,
  query,
  collection,
  where,
  getDocs
} from 'firebase/firestore';
import app from './firebase/config';

export const uploadGarmentsToFirebase = async (garments) => {
  const db = getFirestore(app);
  /* const ss22 = [7613361841415, 7613361841460, 7613361841514, 7613361841569, 7613361841613, 7613361841668, 7613361841422, 7613361841477, 7613361841521, 7613361841576, 7613361841620, 7613361841675, 7613361841712, 7613361841743, 7613361841774, 7613361841804, 7613361841835, 7613361841729, 7613361841750, 7613361841781, 7613361841811, 7613361841842, 7613361945342, 7613361945359, 7613361945366, 7613361945373, 7613361945380, 7613361934872, 7613361934919, 7613361934957, 7613361934995, 7613361935039, 7613361838866, 7613361838880, 7613361838903, 7613361838927, 7613361838941, 7613361838965, 7613361838873, 7613361838897, 7613361838910, 7613361838934, 7613361838958, 7613361838972, 7613361838989, 7613361839016, 7613361839047, 7613361839078, 7613361839108, 7613361838996, 7613361839023, 7613361839054, 7613361839085, 7613361839115, 7613361937316, 7613361937309, 7613361937293, 7613361937323, 7613361937286, 7613361938429, 7613361938405, 7613361938382, 7613361938443, 7613361938467, 7613361937682, 7613361937699, 7613361937705, 7613361937712, 7613361937729, 7613361937781, 7613361937798, 7613361937804, 7613361937811, 7613361937828]; */

  garments.forEach(function (item) {
    db.collection('garments').doc(String(item.uid)).set({
      title: item.title,
      eshop_link: item.eshop_link ? item.eshop_link : null,
      uid: item.uid,
      group_id: item.group_id,
      material: item.eshop_link ? item.material : null,
      product_type: {
        full_type: item.product_type ? item.product_type.full_type : null,
        extracted_type_0: item.product_type ? item.product_type.extracted_type_0 : null,
        extracted_type_1: item.product_type ? item.product_type.extracted_type_1 : null,
        extracted_type_2: item.product_type ? item.product_type.extracted_type_2 : null
      },
      price: item.price ? item.price : null,
      additional_information: {
        custom_label_0: item.additional_information ? item.additional_information.custom_label_0 : null,
        custom_label_1: item.additional_information ? item.additional_information.custom_label_1 : null,
        custom_label_2: item.additional_information ? item.additional_information.custom_label_2 : null,
        custom_label_3: item.additional_information ? item.additional_information.custom_label_3 : null,
        custom_label_4: item.additional_information ? item.additional_information.custom_label_4 : null
      },
      gender: item.gender,
      size: item.size,
      color: item.color ? item.color : '',
      color_code: item.color_code,
      photo: item.photo,
      garment_type: item.garment_type,
      tryon: true,
      static_preview: item.static_preview ? item.static_preview : false
    })
      .then((docRef) => {
        console.log('Garment uploaded successfully!');
      })
      .catch((error) => {
        console.error('Error adding document: ', error);
      });
  });
};

/**
 * Function that updates the Height property in Firebase
 *
 * @param {String} height
 * @param {Object} userData
 */
export const firebaseChangeHeight = async (height, userData) => {
  const doc = await getDoc(userData);

  /**
     * If Size entry exists in Firebase, update the value to the new one.
     */
  if (doc.exists) {
    await updateDoc(userData, { height: height });
  } else {
    /**
       * Else if Size entry does not exist in Firebase, create and set it.
       */
    await setDoc(userData, { height: height });
  }
};

/**
 * Function that updates the Gender property in Firebase
 *
 * @param {Event} event
 * @param {Object} userData
 * @param {String} kind
 */
export const updateFirestoreUserProperty = async (event, userData, kind) => {
  const obj = {};
  obj[kind] = (kind === 'trends_active') ? event.target.checked : event.target.value;

  await setDoc(userData, obj, { merge: true });
};

export const firebaseSetAvatar = async (id, status, userData) => {
  const doc = await getDoc(userData);

  /**
     * If Avatar entry exists in Firebase, update the status to pending, save last scan date, but keep the previous id for now.
     */
  if (doc.exists) {
    await updateDoc(userData, { avatar_status: status, last_scan: id });
  } else {
    /**
       * Else if Avatar entry does not exist in Firebase, create and set, id and status.
       */
    await setDoc(userData, { avatar_id: id, avatar_status: status, last_scan: id });
  }
};

export const deleteCollectionItem = async (kind, created, db, uid) => {
  const url = (kind === 'pending') ? 'collection/user/' + uid + '/output-error-' + created + '.json' : 'collection/user/' + uid + '/output-' + created + '.jpg';
  const userMediaUrl = 'collection/user/' + uid + '/user-media-' + created + '.jpg';

  const storage = getStorage();
  const storageRef = ref(storage, url);
  const storageMediaRef = ref(storage, userMediaUrl);

  // Delete error json or output image
  deleteObject(storageRef).then(() => {
    // console.log(url);
    console.log('Deletion ok');
  }).catch((error) => {
    // console.log(url);
    console.log('Couldnt Delete file: ');
    console.log(error);
  });

  // Delete user media
  deleteObject(storageMediaRef).then(async () => {
    // console.log(userMediaUrl);
    console.log('media delete ok');
  }).catch((error) => {
    // console.log(userMediaUrl);
    console.log('Couldnt Delete media file: ', error);
  });

  // Delete entry
  const q = query(collection(db, 'collection_items/users/' + uid + '/'), where('created', '==', created));
  const querySnapshot = await getDocs(q);
  if (querySnapshot.size > 0) {
    querySnapshot.forEach((doc) => {
      deleteDoc(doc.ref);
      console.log('Firestore entry delete ok');
    });
  }
};

export const getTrendDetectionScore = async (url, demographic) => {
  /*  */
};

export const numDaysBetween = (d1, d2) => {
  const diff = Math.abs(d1.getTime() - d2.getTime());
  return diff / (1000 * 60 * 60 * 24);
};

export const urlToFile = async (url, filename, mimeType) => {
  const res = await fetch(url);
  const buf = await res.arrayBuffer();
  return new File([buf], filename, { type: mimeType });
};
