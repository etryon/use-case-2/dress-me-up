/**
 * @fileOverview The Add a New Collection item page view
 */

import React, { useEffect, useRef, useState } from 'react';
import { makeStyles, useTheme } from '@mui/styles';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import SpeedDial from '@mui/material/SpeedDial';
import SpeedDialAction from '@mui/material/SpeedDialAction';
import ImageList from '@mui/material/ImageList';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Tooltip from '@mui/material/Tooltip';
import FormControl from '@mui/material/FormControl';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import PhotoCameraIcon from '@mui/icons-material/PhotoCamera';
import PhotoLibraryIcon from '@mui/icons-material/PhotoLibrary';
import SendIcon from '@mui/icons-material/Send';
import GetAppIcon from '@mui/icons-material/GetApp';
import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import LayersClearIcon from '@mui/icons-material/LayersClear';
import Backdrop from '@mui/material/Backdrop';
import Snackbar from '@mui/material/Snackbar';
import Grow from '@mui/material/Grow';
import CloseIcon from '@mui/icons-material/Close';
import CloudDoneIcon from '@mui/icons-material/CloudDone';
import Link from '@mui/material/Link';
import Chip from '@mui/material/Chip';
import Paper from '@mui/material/Paper';
import PersonIcon from '@mui/icons-material/Person';
import LocalOfferIcon from '@mui/icons-material/LocalOffer';
// import DeleteIcon from '@mui/icons-material/Delete';
import ReactDOM from 'react-dom';
import { BrowserView, MobileView } from 'react-device-detect';
import Fab from '@mui/material/Fab';
import Camera, { FACING_MODES, IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import ImagePreview from './../components/ImagePreview'; // source code : ./src/demo/AppWithImagePreview/ImagePreview
import CheckStyledRadio from '../components/CheckStyledRadio';
import PleaseSetUserDataScreen from '../components/NoUserDataSplash';
import Box from '@mui/material/Box';
import CardActionArea from '@mui/material/CardActionArea';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import { getFunctions, httpsCallable } from 'firebase/functions';
import LinearWithValueLabel from './../components/LinearProgressWithLabel';
import doImg1 from '../static/do1.png';
import doImg2 from '../static/do2.png';
import doImg3 from '../static/do3.png';
import dontImg1 from '../static/dont1b.png';
import dontImg2 from '../static/dont2.png';
import dontImg3 from '../static/dont3.png';
import CircleIcon from '@mui/icons-material/Circle';
import { doc, setDoc, getFirestore, getDoc, query, collection, where, getDocs } from 'firebase/firestore';
import app, { analytics } from '../firebase/config';
import { useUserAuth } from '../contexts/AuthContext';
import { getStorage, ref, uploadBytesResumable, getDownloadURL } from 'firebase/storage';
import Container from '@mui/material/Container';
import { urlToFile } from '../helper';
import { logEvent } from 'firebase/analytics';
// import { getTrendDetectionScore } from '../helper';
import Resizer from 'react-image-file-resizer';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  title: {
    flexGrow: 1
  },
  gridList: {
    overflow: 'hidden',
    overflowX: 'hidden',
    maxHeight: '70vh',
    width: '100%',
    minWidth: '100%',
    maxWidth: '100vw',
    backgroundColor: theme.palette.background.paper
  },
  extendedIcon: {
    marginRight: theme.spacing(1)
  },
  cameraOverlayStyle: {
    height: '100%',
    position: 'fixed',
    left: 0,
    top: 0,
    overflow: 'hidden',
    zIndex: 1100
  },
  cameraOverlayBackBtn: {
    zIndex: 9999,
    position: 'absolute',
    top: theme.spacing(2),
    left: theme.spacing(2)
  },
  cameraOverlaySendBtn: {
    zIndex: 9999,
    position: 'absolute',
    top: theme.spacing(2),
    right: theme.spacing(4)
  },
  cameraOverlaySaveBtn: {
    zIndex: 9999,
    position: 'absolute',
    top: theme.spacing(2),
    right: theme.spacing(12)
  },
  speedDial: {
    position: 'fixed',
    right: '40px',
    bottom: '10vh'
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff'
  },
  chipArray: {
    display: 'flex',
    flexWrap: 'wrap',
    listStyle: 'none',
    padding: theme.spacing(0.5),
    margin: 0
  },
  chip: {
    margin: theme.spacing(1)
  }
}));

/**
 * A function that returns an array of strings that feature all upload steps.
 * @memberOf New
 * @returns {string[]} An array of strings
 */
function getStepperSteps () {
  return ['Select a photo', 'Select a garment', 'Upload a new item'];
}

/**
 * A function that returns the string that describes each upload step.
 * @memberOf New
 * @param {Number} stepIndex The current step index id.
 * @returns {string} The current step string.
 */
function getStepperStepContent (stepIndex) {
  switch (stepIndex) {
    case 0:
      return 'Select a photo';
    case 1:
      return 'Select a garment';
    case 2:
      return 'Upload a new item';
    default:
      return 'Unknown Step';
  }
}

/**
 * Component that creates the New collection item view.
 * @namespace New
 * @returns {JSX.Element}
 */
function New () {
  const classes = useStyles();
  const theme = useTheme();

  const user = useUserAuth().user;
  const db = getFirestore(app);

  const [tileData, setTileData] = useState(null);

  const inputFile = useRef(null);
  /**
   * Opens File Dialog popup to select a media file from the filesystem.
   */
  const openFileDialog = () => {
    inputFile.current.click();
  };

  /**
   * Event Handler that fires when a new image to upload is selected.
   * @todo This is only working for an image. Add use case for video
   * @param {Event} event - Event when a media file is loaded from a local file
   */
  const onInputFileChange = (event) => {
    const file = event.target.files[0];

    Resizer.imageFileResizer(
      file, // Is the file of the image which will resized.
      1368, // Is the maxWidth of the resized new image.
      1368, // Is the maxHeight of the resized new image.
      'JPEG', // Is the compressFormat of the resized new image.
      95, // Is the quality of the resized new image.
      0, // Is the degree of clockwise rotation to apply to uploaded image.
      (uri) => {
        updateMediaGrid(uri);
      },
      'base64'
    );
  };

  const [selectedMedia, setSelectedMedia] = useState(null);
  /**
   * Event Handler that fires when a new media file is selected and updates State.
   * @param {Event} event - Event when a media file is selected
   */
  const handleMediaSelection = (event) => {
    setSelectedMedia(event.target.value);
    console.log(event.target.value);
  };

  const [garments, setGarments] = useState([]);
  const [selectedGarment, setSelectedGarment] = useState(null);
  const [selectedGarmentId, setSelectedGarmentId] = useState(null);
  /**
   * Event Handler that fires when a new garment is selected and updates State.
   * @param {Event} event - Event when a garment is selected
   */
  const handleGarmentSelection = (event) => {
    setSelectedGarment(garments.find(x => x.uid === event.target.value));
    setSelectedGarmentId(event.target.value);
  };

  /**
   * activeStep state controls the whole view.
   */
  const [activeStep, setActiveStep] = useState(0);
  const steps = getStepperSteps();
  /**
   * Sets Active Step when a Next Step event is fired.
   */
  const handleStepperNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };
  /**
   * Sets Active Step when a Previous Step event is fired.
   */
  const handleStepperBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };
  /**
   * Sets Active Step when a Reset Step event is fired.
   */
  const handleStepperReset = () => {
    setActiveStep(0);
  };

  const [fabOpen, setFabOpen] = useState(false);
  /**
   * Closes FAB Speed dial
   */
  const handleSpeedDialClose = () => {
    setFabOpen(false);
  };
  /**
   * Opens FAB Speed dial
   */
  const handleSpeedDialOpen = () => {
    setFabOpen(true);
  };

  /**
   * Renders the Mobile Camera Component
   */
  const openCameraOverlay = () => {
    const element = <MobileCameraComponent/>;
    ReactDOM.render(element, document.getElementById('cameraOverlay'));
  };

  /**
   * The available Speed Dial actions that are shown when the component is open.
   * @type {Array.<{icon: JSX.Element, name: String, event: Event}>}
   */
  const fabActions = [
    { icon: <PhotoCameraIcon />, name: 'From Camera', event: openCameraOverlay },
    { icon: <PhotoLibraryIcon />, name: 'From File', event: openFileDialog }
  ];

  /**
   * Updates media grid with all available media files and updates state with selected media file
   * @param {string} media Base64 encoded image string
   */
  const updateMediaGrid = (media) => {
    const lastId = tileData ? parseInt(Math.max.apply(Math, tileData.map(function (o) { return o.id; })), 10) : 0;

    const newEntry = {
      img: media,
      title: 'Image' + (lastId + 1),
      cols: 1,
      id: (lastId + 1)
    };

    if (tileData) {
      setTileData([newEntry, ...tileData]);
    } else {
      setTileData([newEntry]);
    }
    setSelectedMedia(newEntry.title);
  };

  const [openBackdrop, setOpenBackdrop] = useState(false);
  /**
   * Hides backdrop.
   */
  const handleCloseBackdrop = () => {
    setOpenBackdrop(false);
  };

  const [snackbar, setSnackbar] = useState(false);
  const [transition, setTransition] = useState(undefined);
  const [snackbarMessage, setSnackbarMessage] = useState('');

  const [uploadProgress, setUploadProgress] = useState(0);

  /**
   * Hides Snackbar component.
   */
  const handleSnackbarClose = () => {
    setSnackbar(false);
  };

  /**
   * Sets the direction of the Speed Dial menu
   * @param {Object} props
   * @returns {JSX.Element}
   * @constructor
   */
  function TransitionUp (props) {
    return <Grow {...props} direction="up" />;
  }

  /**
   * Handles the upload of a new collection item.
   * @param {Event} e - Event that is fired when the submission of a new collection item starts.
   * @todo Change media item selection by 'title' to a more concrete way.
   * @returns {Promise<void>}
   */
  const handleUpload = async (e) => {
    e.preventDefault();

    setOpenBackdrop(true);

    const mediaItem = tileData.find(x => x.title === selectedMedia).img;
    const uid = user.uid;
    const storage = getStorage();
    const timestamp = Date.now();
    const uniqueName = 'user-media-' + timestamp + '.jpg';

    const mediaRef = ref(storage, 'collection/user/' + uid + '/' + uniqueName);

    // Convert base64 string to file
    const file = await urlToFile(
      mediaItem
    );

    const uploadTask = uploadBytesResumable(mediaRef, file);

    uploadTask.on('state_changed',
      (snapshot) => {
        // Observe state change events such as progress, pause, and resume
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        setUploadProgress(Math.trunc(progress));
        switch (snapshot.state) {
          case 'paused':
            console.log('Upload is paused');
            break;
          case 'running':
            console.log('Upload is running');
            break;
        }
      },
      (error) => {
        handleCloseBackdrop();
        console.log(error);

        switch (error.code) {
          case 'storage/unauthorized':
            setSnackbarMessage("User doesn't have permission to access the object");
            setTransition(() => TransitionUp);
            setSnackbar(true);
            break;
          case 'storage/canceled':
            setSnackbarMessage('User canceled the upload');
            setTransition(() => TransitionUp);
            setSnackbar(true);
            break;
          case 'storage/unknown':
            setSnackbarMessage('Unknown error occurred, inspect error.serverResponse');
            setTransition(() => TransitionUp);
            setSnackbar(true);
            break;
          default:
            setSnackbarMessage('Unknown error');
            setTransition(() => TransitionUp);
            setSnackbar(true);
        }
      },
      () => {
        // Handle successful uploads on complete
        getDownloadURL(uploadTask.snapshot.ref).then(async (downloadURL) => {
          // console.log('File available at', downloadURL);

          const entryData = {
            name: selectedGarment.color ? selectedGarment.title + ' - ' + selectedGarment.color : selectedGarment.title,
            created: String(timestamp),
            status: 'pending',
            garmentId: selectedGarment.uid,
            groupId: selectedGarment.group_id,
            user_media: uniqueName,
            output: null,
            type: 'photo'
          };

          // Add a new document with a generated id
          const newDocRef = doc(collection(db, 'collection_items/users/' + uid));
          await setDoc(newDocRef, entryData);
          console.log('Document written with ID: ', newDocRef);

          /**
             * Now we can call the Dress Me Up Composition Invoker cloud function
             */
          const cloudFunctions = getFunctions(
            undefined,
            process.env.REACT_APP_CLOUD_FUNCTIONS_REGION
          );
          const compositionInvoker = httpsCallable(cloudFunctions, 'image-composition');

          const userDataRef = doc(db, 'user_info', user.uid);
          const userDoc = await getDoc(userDataRef);
          if (userDoc.exists()) {
            const avatarId = userDoc.data().avatar_id;
            const avatarGender = userDoc.data().avatar_gender;

            const invokeCompositionService = async () => {
              try {
                const data = {
                  group_id: String(selectedGarment.group_id),
                  colorway: String(selectedGarment.color_code),
                  garment_size: String(selectedGarment.size),
                  avatar_id: String(avatarId),
                  avatar_gender: String(avatarGender),
                  user_media: String(uniqueName),
                  warp_region: String(selectedGarment.garment_type)
                };

                // console.log(data);

                // setMintedUrlData(result);
                return await compositionInvoker(data);
              } catch (e) {
                console.log(e);
              }
            };

            const invokeResult = await invokeCompositionService();
            if (invokeResult) {
              logEvent(analytics, 'collection_item_upload',
                {
                  group_id: parseInt(selectedGarment.group_id, 10),
                  colorway: parseInt(selectedGarment.color_code, 10),
                  garment_size: String(selectedGarment.size),
                  avatar_id: parseInt(avatarId, 10),
                  warp_region: String(selectedGarment.garment_type)
                });
              handleCloseBackdrop();
              setSnackbarMessage('Collection Item uploaded successfully!');
              setTransition(() => TransitionUp);
              setSnackbar(true);
              handleStepperNext();
            } else {
              handleCloseBackdrop();
              setSnackbarMessage('There was an issue uploading your Collection Item. Please try again later. (invoke result).');
              setTransition(() => TransitionUp);
              setSnackbar(true);
            }
          }
        }).catch((error) => {
          handleCloseBackdrop();

          console.error('Error adding document: ', error);

          setSnackbarMessage('There was an issue uploading your Collection Item. Please try again later.');
          setTransition(() => TransitionUp);
          setSnackbar(true);
        });
      }
    );
  };

  const [chipData, setChipData] = useState([]);

  /**
   * Loads all garments from Firebase
   * @returns {Promise<void>}
   */
  const fetchGarments = async (db, user) => {
    try {
      const userDataRef = doc(db, 'user_info', user.uid);
      const userDoc = await getDoc(userDataRef);

      const gender = userDoc.data().garment_gender;
      const size = userDoc.data().size;
      const avatarId = userDoc.data().avatar_id;
      if (!avatarId) {
        return;
      }

      // const trendsEnabled = userDoc.data().trends_active;
      // const demographicGender = trendsEnabled ? doc.data().demographic_gender : '';
      // const demographicAge = trendsEnabled ? doc.data().demographic_age : '';
      const chipData = [
        { key: 0, tooltip: 'Type', label: gender.charAt(0).toUpperCase() + gender.slice(1), icon: <PersonIcon /> },
        { key: 1, tooltip: 'Size', label: size.toUpperCase(), icon: <LocalOfferIcon /> }
      ];
      setChipData(chipData);

      const q = query(collection(db, 'garments'), where('size', '==', size), where('gender', '==', gender));
      const querySnapshot = await getDocs(q);
      if (querySnapshot.size > 0) {
        const allDocs = [];
        querySnapshot.forEach((doc) => {
          const data = doc.data();
          allDocs.push(data);
        });

        // Sort by group id
        allDocs.sort((a, b) => (a.group_id < b.group_id) ? 1 : ((b.group_id < a.group_id) ? -1 : 0));

        setGarments(allDocs);
      } else {
        console.log('no docs found');
      }
    } catch (error) {
      console.log('error', error);
    }
  };

  useEffect(() => {
    if (Object.keys(user).length !== 0) {
      if (garments.length === 0) {
        fetchGarments(db, user).then(() => {});
      } else {
        console.log('No such document!');
      }
    }
  }, [user]);

  return (
      <>
        <Container maxWidth="md" sx={{ padding: 0, width: '100%', overflowX: 'hidden', overflowY: 'auto' }}>
          <Backdrop className={classes.backdrop} open={openBackdrop} /* onClick={handleCloseBackdrop} */ >
            <LinearWithValueLabel value = {{ uploadProgress }}/>
          </Backdrop>

          <Snackbar
              open={snackbar}
              onClose={handleSnackbarClose}
              TransitionComponent={transition}
              message={snackbarMessage}
              autoHideDuration={5000}
              key={transition ? transition.name : ''}
              action={
                <React.Fragment>
                  <IconButton size="small" aria-label="close" color="inherit" onClick={handleSnackbarClose}>
                    <CloseIcon fontSize="small" />
                  </IconButton>
                </React.Fragment>
              }
          />

          {
            /**
             * If no user data are set, then show 'No User Data set up' view.
             */
            (chipData.length === 0)

              ? <PleaseSetUserDataScreen/>

              : <>
                  <AppBar position="static" color='primary'>
                    <Toolbar>

                      {activeStep === steps.length
                        ? (
                              <>
                                <Typography variant="h6" className={classes.title}>Upload complete!</Typography>
                                <Button onClick={handleStepperReset} variant="contained" color="secondary">Upload another</Button>
                              </>
                          )
                        : (
                              <>

                                { activeStep !== 0
                                  ? (
                                        <Button edge="start"
                                                onClick={handleStepperBack}
                                                color="inherit"
                                        >
                                          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                                          Back
                                        </Button>)
                                  : null }

                                <Typography variant="h6" className={classes.title}>{getStepperStepContent(activeStep)}</Typography>

                                { selectedMedia && activeStep === 0
                                  ? (

                                        <Button variant="contained" color="secondary"
                                             onClick={handleStepperNext}>
                                          {activeStep === steps.length - 1 ? '' : 'Next'}
                                          {activeStep === steps.length - 1 ? null : theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight /> }
                                        </Button>
                                    )
                                  : ''
                                }

                                { selectedGarment && activeStep === 1
                                  ? (

                                        <Button
                                            variant="contained" color="secondary" onClick={handleStepperNext}>
                                          {activeStep === steps.length - 1 ? '' : 'Next'}
                                          {activeStep === steps.length - 1 ? null : theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight /> }
                                        </Button>
                                    )
                                  : ''
                                }
                              </>
                          )}

                    </Toolbar>
                  </AppBar>
                  <Box sx={{ mt: 2, mb: 1 }}>
                    <Stepper activeStep={activeStep} alternativeLabel>
                      {steps.map((label) => (
                          <Step key={label}>
                            {/* <StepLabel>{label}</StepLabel> */}
                            <StepLabel/>
                          </Step>
                      ))}
                    </Stepper>
                  </Box>
                  {
                    activeStep === 0
                      ? <>
                          <div id="cameraOverlay"/>
                          <div style={{ position: 'relative' }}>
                            { (tileData)
                              ? <FormControl component="fieldset" >
                                  <RadioGroup aria-label="media" name="media-item-radio" value={selectedMedia} onChange={handleMediaSelection} sx={{ paddingLeft: '8px', paddingRight: '8px' }}>
                                    <ImageList rowHeight={280} className={classes.gridList} cols={ tileData.length === 1 ? 1 : 2 } gap={16} >

                                      {
                                        tileData.map((tile) => (

                                            <Card key={tile.img} sx={{ marginBottom: 2 }}>

                                              <CardActionArea>

                                                <FormControlLabel style={{ width: '100%', height: '100%', display: 'block', position: 'absolute' }}
                                                                  value={tile.title}
                                                                  control={
                                                                    <CheckStyledRadio sx={{ position: 'absolute', left: 24, top: 20 }} disableRipple />
                                                                  } label=''/>
                                                <CardMedia
                                                    component="img"
                                                    height="360px"
                                                    style={{ objectFit: 'contain' }}
                                                    image={tile.img}
                                                    alt={tile.title}
                                                >

                                                </CardMedia>
                                              </CardActionArea>
                                             {/* <IconButton aria-label="delete">
                                                <DeleteIcon/>
                                              </IconButton> */}
                                            </Card>
                                        )) }

                                    </ImageList>
                                  </RadioGroup>
                                </FormControl>
                              : <>
                                  <Grid container spacing={2} sx={{ pl: 2, pr: 2 }}>
                                    <Grid item xs={6} sm={4} md={4} lg={4} xl={4}>
                                      <Box
                                          component="img"
                                          aspectratio={(1)}
                                          sx={{
                                            objectFit: 'contain',
                                            width: '100%',
                                            position: 'static'
                                          }}
                                          src={ doImg1 }
                                      />
                                      <Box sx={{ borderTop: 5, borderColor: 'limegreen' }}>
                                        <Typography color='#2e7b32' variant='subtitle2' sx={{ display: 'block', textAlign: 'left' }}>
                                          Do
                                        </Typography>
                                        <Typography variant='body2' gutterBottom sx={{ display: 'block', textAlign: 'left' }}>
                                          Keep your entire body inside the photo, including your feet!
                                        </Typography>
                                      </Box>
                                    </Grid>

                                    <Grid item xs={6} sm={4} md={4} lg={4} xl={4}>
                                      <Box
                                          component="img"
                                          aspectratio={(1)}
                                          sx={{
                                            objectFit: 'contain',
                                            width: '100%',
                                            position: 'static'
                                          }}
                                          src={ doImg2 }
                                      />
                                      <Box sx={{ borderTop: 5, borderColor: 'limegreen' }}>
                                        <Typography color='#2e7b32' variant='subtitle2' sx={{ display: 'block', textAlign: 'left' }}>
                                          Do
                                        </Typography>
                                        <Typography variant='body2' gutterBottom sx={{ display: 'block', textAlign: 'left' }}>
                                          Try various poses while facing the camera.
                                        </Typography>
                                      </Box>
                                    </Grid>

                                    <Grid item xs={6} sm={4} md={4} lg={4} xl={4}>
                                      <Box
                                          component="img"
                                          aspectratio={(1)}
                                          sx={{
                                            objectFit: 'contain',
                                            width: '100%',
                                            position: 'static'
                                          }}
                                          src={ doImg3 }
                                      />
                                      <Box sx={{ borderTop: 5, borderColor: 'limegreen' }}>
                                        <Typography color='#2e7b32' variant='subtitle2' sx={{ display: 'block', textAlign: 'left' }}>
                                          Do
                                        </Typography>
                                        <Typography variant='body2' gutterBottom sx={{ display: 'block', textAlign: 'left' }}>
                                          Capture your photo in well lit environments.
                                        </Typography>
                                      </Box>
                                    </Grid>

                                    <Grid item xs={6} sm={4} md={4} lg={4} xl={4}>
                                      <Box
                                          component="img"
                                          aspectratio={(1)}
                                          sx={{
                                            objectFit: 'contain',
                                            width: '100%',
                                            position: 'static'
                                          }}
                                          src={ dontImg1 }
                                      />
                                      <Box sx={{ borderTop: 5, borderColor: 'orangered' }}>
                                        <Typography color='#d32f2f' variant='subtitle2' sx={{ display: 'block', textAlign: 'left' }}>
                                          Avoid
                                        </Typography>
                                        <Typography variant='body2' gutterBottom sx={{ display: 'block', textAlign: 'left' }}>
                                          Wearing baggy or drapey clothes.
                                        </Typography>
                                      </Box>
                                    </Grid>

                                    <Grid item xs={6} sm={4} md={4} lg={4} xl={4}>
                                      <Box
                                          component="img"
                                          aspectratio={(1)}
                                          sx={{
                                            objectFit: 'contain',
                                            width: '100%',
                                            position: 'static'
                                          }}
                                          src={ dontImg2 }
                                      />
                                      <Box sx={{ borderTop: 5, borderColor: 'orangered' }}>
                                        <Typography color='#d32f2f' variant='subtitle2' sx={{ display: 'block', textAlign: 'left' }}>
                                          Avoid
                                        </Typography>
                                        <Typography variant='body2' gutterBottom sx={{ display: 'block', textAlign: 'left' }}>
                                          Complex poses or crossed extremities.
                                        </Typography>
                                      </Box>
                                    </Grid>

                                    <Grid item xs={6} sm={4} md={4} lg={4} xl={4}>
                                      <Box
                                          component="img"
                                          aspectratio={(1)}
                                          sx={{
                                            objectFit: 'contain',
                                            width: '100%',
                                            position: 'static'
                                          }}
                                          src={ dontImg3 }
                                      />
                                      <Box sx={{ borderTop: 5, borderColor: 'orangered' }}>
                                        <Typography color='#d32f2f' variant='subtitle2' sx={{ display: 'block', textAlign: 'left' }}>
                                          Avoid
                                        </Typography>
                                        <Typography variant='body2' gutterBottom sx={{ display: 'block', textAlign: 'left' }}>
                                          Although friendship is invaluable, having your friend inside the frame will not work.
                                        </Typography>
                                      </Box>
                                    </Grid>

                                  </Grid>
                                </>
                            }

                            <MobileView style={{ height: '100%' }}>
                              <Fab sx={{ right: '40px', bottom: '10vh', position: 'fixed' }}
                                   color='secondary' size='large' aria-label='add' onClick={openFileDialog}>
                                <PhotoCameraIcon />
                              </Fab>
                            </MobileView>

                            <BrowserView style={{ height: '100%' }}>
                              <SpeedDial

                                  ariaLabel="Upload a photo"
                                  className={classes.speedDial}
                                  icon={
                                    <Box sx={{ display: 'flex' }}>
                                    <Typography> Add a photo </Typography>
                                    </Box>
                                  }
                                  onClose={handleSpeedDialClose}
                                  onOpen={handleSpeedDialOpen}
                                  open={fabOpen}
                                  FabProps={{ color: 'secondary', size: 'large', variant: 'extended' }}
                                  direction={'left'}
                              >
                                {fabActions.map((action) => (
                                    <SpeedDialAction title={action.name}
                                                     key={action.name}
                                                     icon={action.icon}
                                                     tooltipTitle={action.name}
                                                     onClick={action.event}
                                    />
                                ))}

                              </SpeedDial>
                            </BrowserView>
                          </div>
                          <input type='file'
                                 accept=".jpg, .jpeg, .png"
                                 id="media-item-file"
                                 ref={inputFile}
                                 onChange={onInputFileChange}
                                 style={{ display: 'none' }}
                          />

                        </>
                      : null
                  }

                  { activeStep === 1
                    ? <>

                        <Paper component="ul" className={classes.chipArray} elevation={0}>
                          <li>
                            <Typography variant="h6" component="h6" style={{ lineHeight: '2.1' }}>
                              Active filters: &nbsp;
                            </Typography>
                          </li>
                          {
                            chipData.map((data) => (
                                <li key={data.key}>
                                  <Tooltip title={data.tooltip}>
                                    <Chip
                                        icon={data.icon}
                                        label={data.label}
                                        /* onDelete={data.label === 'React' ? undefined : handleDelete(data)} */
                                        className={classes.chip}
                                    />
                                  </Tooltip>
                                </li>
                            ))
                          }
                        </Paper>

                        { (garments)

                          ? <FormControl component="fieldset">
                              <RadioGroup aria-label="garment" name="garment-item-radio" value={selectedGarmentId}
                                          onChange={handleGarmentSelection} sx={{ paddingLeft: '8px', paddingRight: '8px' }}>

                                <ImageList className={classes.gridList} gap={12} style={{ transform: 'translateZ(0)' }} cols={2}>

                                  {
                                    garments.map((garment) => (
                                        <Card key={garment.uid} sx={{ marginBottom: 2, overflow: 'visible' }}>
                                          <CardActionArea>
                                            {
                                              (garment.color)
                                                ? <Chip label={garment.color} size="small" color="primary" variant="filled" sx={{ position: 'absolute', right: 12, top: 28 }} />
                                                : ''
                                            }
                                            <FormControlLabel style={{ width: '100%', height: '100%', display: 'block', position: 'absolute' }}
                                                              value={garment.uid}
                                                              control={
                                                                <CheckStyledRadio sx={{ position: 'absolute', left: 20, top: 20 }} disableRipple />
                                                              } label=''/>
                                            <CardMedia
                                                component="img"
                                                height="100%"
                                                image={garment.photo}
                                                alt={garment.title}
                                            >
                                            </CardMedia>

                                            <CardContent>
                                              <Typography variant="body2" color="text.secondary">
                                                {garment.title}
                                              </Typography>
                                            </CardContent>
                                          </CardActionArea>
                                        </Card>
                                    ))}
                                </ImageList>
                              </RadioGroup>
                            </FormControl>
                          : <Grid container style={{ minHeight: '70vh' }} align="center" justifyContent="center" alignItems="center" >
                              <Grid item xs={12} style={{ textAlign: 'center' }}>
                                <LayersClearIcon color='disabled' style={{ fontSize: 128 }} />
                                <Typography color='textSecondary' variant="h6" gutterBottom>
                                  No garments available
                                </Typography>
                              </Grid>
                            </Grid>
                        }

                      </>

                    : null }

                  {
                    activeStep === 2
                      ? <form autoComplete="off" onSubmit={handleUpload}>

                          <Grid container sx={{ pl: 2, pr: 2 }} justifyContent="center" spacing={2} >

                            <Grid item xs={6} sm={6}>
                              <Typography variant="h6" gutterBottom>
                                Photo
                              </Typography>

                              <Paper
                                  component="img"
                                  id='selected-media-item' aspectratio={(1)}
                                  sx={{
                                    objectFit: 'contain',
                                    width: '100%',
                                    height: '39vh',
                                    padding: 0,
                                    position: 'static'
                                  }}
                                  src={
                                    (typeof selectedMedia === 'string') ? tileData.find(x => x.title === selectedMedia).img : ''
                                  }
                              />

                            </Grid>

                            <Grid item xs={6} sm={6}>
                              <Typography variant="h6" gutterBottom>
                                Garment
                              </Typography>

                              <Paper
                                  component="img"
                                  aspectratio={(1)} style={{
                                    objectFit: 'contain',
                                    width: '100%',
                                    height: '39vh',
                                    padding: 0,
                                    position: 'static'
                                  }}
                                  src={
                                    selectedGarment.photo
                                  }
                              />

                              <Typography variant="p" gutterBottom style={{ textAlign: 'left' }}>
                                {
                                  selectedGarment.title
                                }
                                {
                                  (selectedGarment.color) ? ' - ' + selectedGarment.color : ''
                                }
                              </Typography>
                            </Grid>

                            <Grid item xs={12} sm={10} md={8}>
                              <Box sx={{ mb: 2 }}>
                              <Button type="submit" variant="contained" color="secondary" size="large" id='upload-button' fullWidth>
                                UPLOAD
                              </Button>
                              </Box>
                            </Grid>

                          </Grid>
                        </form>
                      : null
                  }
                  {
                    activeStep === 3
                      ? <Grid container style={{ minHeight: '70vh' }} align="center" justifyContent="center" alignItems="center">
                          <Grid item xs={12} style={{ textAlign: 'center' }}>
                            <CloudDoneIcon color='disabled' style={{ fontSize: 128 }} />
                            <Typography color='textSecondary' variant="h6" gutterBottom>
                              Your new collection item is now processing!
                            </Typography>
                            <Typography color='textSecondary' variant="h6" gutterBottom>
                              It takes less than 30 minutes to complete.
                            </Typography>
                            <Typography color='textSecondary' variant="h6" gutterBottom>
                              You can see its status in your&nbsp;
                              <Link href="collection" to="/collection">
                                Collection
                              </Link>.
                            </Typography>
                          </Grid>
                        </Grid>
                      : null
                  }
                </>
          }
        </Container>
      </>
  );

  /**
   * The Mobile / Web camera component, that handles capturing a new photo.
   * @namespace MobileCameraComponent
   * @param {Object} props
   * @returns {JSX.Element}
   */
  function MobileCameraComponent (props) {
    const [dataUri, setDataUri] = useState('');
    const [cameraCountdownValue, setCameraCountdownValue] = useState('');
    const [backButtonVisibility, setBackButtonVisibility] = useState('hidden');

    /**
     * Event Handler when the capture event fires
     * @memberOf MobileCameraComponent
     * @param {String} dataUri
     */
    const handleTakePhotoAnimationDone = (dataUri) => {
      console.log('takePhotoAnimationDone');
      setDataUri(dataUri);
    };

    /**
     * Camera capture error event handler
     * @memberOf MobileCameraComponent
     * @param {Object} error - The error object
     */
    const handleCameraError = (error) => {
      console.log('handleCameraError', error);
    };

    /**
     * Camera capture start event handler
     * @memberOf MobileCameraComponent
     * @param stream
     */
    const handleCameraStart = (stream) => {
      setBackButtonVisibility('visible');
      console.log('handleCameraStart');
    };

    /**
     * Camera capture stop event handler
     * @memberOf MobileCameraComponent
     * @param {String} dataUri
     */
    const handleCameraStop = (dataUri) => {
      setBackButtonVisibility('hidden');
      console.log('handleCameraStop');
      setDataUri(dataUri);
    };

    /**
     * Camera event handler for the 'back / close' event
     * @memberOf MobileCameraComponent
     */
    const handleCameraBack = () => {
      console.log('handleCameraBack');
      setDataUri(null);
    };

    /**
     * Event handler that fires when the Image can be saved to the Grid.
     * @memberOf MobileCameraComponent
     */
    const handleCameraSaveImageToGrid = () => {
      updateMediaGrid(dataUri);
      setDataUri(null);
    };

    /**
     * Save Photo to local (download) event handler
     * @memberOf MobileCameraComponent
     */
    const handleSavePhoto = () => {
      const link = document.createElement('a');
      link.href = dataUri;
      link.setAttribute('download', 'image.png'); // or any other extension
      document.body.appendChild(link);
      link.click();
    };

    const customCapture = (n) => setTimeout(() => {
      setBackButtonVisibility('hidden');
      if (n) {
        setCameraCountdownValue((n));
        return customCapture(n - 1);
      }
      setCameraCountdownValue('');
      document.querySelector('#outer-circle').click();
    }, 1000);

    return (
        <>
          {
            (dataUri)
              ? <>
                  {
                    (typeof dataUri === 'string')
                      ? <div className={classes.cameraOverlayStyle}>
                          <ImagePreview dataUri={dataUri}
                                        isFullscreen={true}
                          />
                          <Box className={classes.cameraOverlayBackBtn}>
                            <Tooltip title="Back" aria-label="back">
                              <IconButton aria-label="back"
                                          onClick={handleCameraBack}
                              >
                                <ArrowBackIcon fontSize={'large'} />
                              </IconButton>
                            </Tooltip>
                          </Box>

                          <Box className={classes.cameraOverlaySendBtn}>
                            <Tooltip title="Send" aria-label="send">
                              <IconButton aria-label="send"
                                          color="secondary"
                                          onClick={handleCameraSaveImageToGrid}
                              >
                                <SendIcon fontSize={'large'} />
                              </IconButton>
                            </Tooltip>
                          </Box>

                          <Box className={classes.cameraOverlaySaveBtn}>
                            <Tooltip title="Save" aria-label="save">
                              <IconButton aria-label="save"
                                          onClick={handleSavePhoto}
                              >
                                <GetAppIcon fontSize={'large'} />
                              </IconButton>
                            </Tooltip>
                          </Box>

                        </div>
                      : null
                  }
                </>
              : <>
                  {
                    (dataUri === '')
                      ? <div className={classes.cameraOverlayStyle}>
                          <Camera
                              onTakePhotoAnimationDone = { (dataUri) => { handleTakePhotoAnimationDone(dataUri); } }
                              onCameraError = { (error) => { handleCameraError(error); } }
                              idealFacingMode = {FACING_MODES.ENVIRONMENT}
                              imageType = {IMAGE_TYPES.JPG}
                              imageCompression = {0.97}
                              isMaxResolution = {true}
                              isImageMirror = {false}
                              isSilentMode = {false}
                              isDisplayStartCameraError = {true}
                              isFullscreen = {true}
                              sizeFactor = {1}
                              onCameraStart = { (stream) => { handleCameraStart(stream); } }
                              onCameraStop = { () => { handleCameraStop(dataUri); } }
                          />
                          <Box className={classes.cameraOverlayBackBtn} sx={{ visibility: backButtonVisibility }}>
                            <Tooltip title="Back" aria-label="back">
                              <IconButton aria-label="back"
                                          onClick={handleCameraStop}
                              >
                                <ArrowBackIcon fontSize={'large'} />
                              </IconButton>
                            </Tooltip>
                          </Box>

                          <Box sx={{ position: 'absolute', left: '50%', bottom: '50%', marginLeft: '-48px' }}>
                            <Typography variant="h1" sx={{ color: 'white' }}>
                              {cameraCountdownValue}
                            </Typography>
                          </Box>

                          <Box sx={{ position: 'absolute', left: '50%', bottom: '90px', marginLeft: '-64px' }}>
                            <Tooltip title="Capture (5 sec delay)" aria-label="capture">
                              <IconButton aria-label="back"
                                          onClick={ () => customCapture(5) }
                              >
                                <CircleIcon sx={{ color: 'white', fontSize: 64 }} />
                              </IconButton>
                            </Tooltip>
                          </Box>

                        </div>
                      : null
                  }
                </>
          }
        </>
    );
  }
}

export default New;
