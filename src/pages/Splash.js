/**
 * @fileOverview The Splash page view
 */

import React, { useEffect, useState } from 'react';
import logo from './../static/logo.png';
import Grid from '@mui/material/Grid';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { pink } from '@mui/material/colors';
import GoogleButton from 'react-google-button';
import { useUserAuth } from '../contexts/AuthContext';
import { useNavigate } from 'react-router';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Chip from '@mui/material/Chip';
import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Typography from '@mui/material/Typography';
import licensePdf from '../static/Licence-Agreement_Terms-and-conditions.pdf';
import Alert from '@mui/material/Alert';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
    primary: {
      main: pink[500]
    },
    secondary: {
      main: '#ffffff'
    }
  }
});

/**
 * Component for showing the login / signup screen of the app.
 *
 * @namespace Splash
 * @returns {JSX.Element} The Splash view for logging in the application.
 */
function Splash () {
  const [email, setEmail] = useState('');
  const navigate = useNavigate();
  const { user, googleSignIn, sendSignInEmail, authenticateWithEmail } = useUserAuth();
  const [componentDisabled, setComponentDisabled] = useState(false);
  const [snackbar, setSnackbar] = useState(false);
  const [snackbarMsg, setSnackbarMsg] = useState('');

  authenticateWithEmail();

  const handleSnackbarClose = () => {
    setSnackbar(false);
    setSnackbarMsg('');
  };

  const handleGoogleSignIn = async () => {
    try {
      await googleSignIn();
      navigate('/collection');
    } catch (error) {
      console.log(error.message);
      setSnackbarMsg(error.message);
      setSnackbar(true);
    }
  };

  const handleEmailSignIn = async () => {
    try {
      await sendSignInEmail(email);
      window.localStorage.setItem('emailForSignIn', email);

      setComponentDisabled(true);
      setSnackbarMsg('Your magic link has been sent to your e-mail address. Please check your Inbox and Spam folders!');
      setSnackbar(true);
    } catch (err) {
      console.log('error: ', err.code);

      switch (err.code) {
        case 'auth/missing-email':
          setSnackbarMsg('Please enter your e-mail.');
          break;

        case 'auth/invalid-email':
          setSnackbarMsg('Please enter a valid e-mail address.');
          break;

        default :
          setSnackbarMsg('Error creating link. Please try again later.');
          break;
      }
      setSnackbar(true);
    }
  };

  const downloadLicenseAggreement = () => {
    const link = document.createElement('a');
    link.download = 'eTryOn-license.pdf';
    link.href = licensePdf;
    link.click();
  };

  useEffect(() => {
    if (user) {
      navigate('/collection');
    }
  }, [user]);

  return (
        <Box sx={{ backgroundColor: '#173D5D', height: '100vh', overflowX: 'hidden' }}>

            <Snackbar
                open={snackbar}
                message={snackbarMsg}
                action={
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleSnackbarClose}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                }
            >
            </Snackbar>

            <Container maxWidth="md" >
                <ThemeProvider theme={darkTheme} >
                    <Grid container justifyContent="center" alignItems="center" spacing={6} rowSpacing={4}>

                        <Grid item xs={12}>
                            <img src={logo} alt="Dress Me Up logo" className='App-logo'/>
                            <h1 className='AppTitleStyle'> DRESS ME UP </h1>
                        </Grid>

                        <Grid item xs={12}>
                            <Box sx={{ mb: 2 }}>
                                <Alert variant="filled" severity="info">
                                    If it&apos;s your first time using the app, please login with your mobile phone and ask a friend to hold your phone to scan your body!
                                </Alert>
                            </Box>
                        </Grid>

                        <Grid item xs={12}>
                            <Box sx={{ display: 'inline-flex', mb: 2 }}>
                                <GoogleButton className="g-btn"
                                              type="dark"
                                              onClick={ handleGoogleSignIn }
                                />
                            </Box>
                            <Box>
                                <Typography variant='caption' color='white'>OR</Typography>
                            </Box>
                        </Grid>

                        <Grid item xs={12}>
                            <Box sx={{ mb: 2 }}>
                                <Divider sx={{ mb: 2 }}>
                                    <Chip label="Sign in with e-mail link" />
                                </Divider>
                                <Typography variant='caption' color='white'>The magic link is valid only for the device / browser you created it from.</Typography>
                            </Box>
                            <Box sx={{ mb: 2 }}>
                                <FormControl variant="outlined">
                                    <TextField sx={{ width: '38ch' }}
                                               id="email-input-text"
                                               label="E-mail"
                                               variant="outlined"
                                               color="primary"
                                               fullWidth
                                               type="email"
                                               disabled={ componentDisabled }
                                               onBlur={ (e) => setEmail(e.target.value) }
                                    />
                                </FormControl>
                            </Box>
                            <Box sx={{ mb: 2 }}>
                                <Button
                                    variant="outlined" color="secondary" size="large" id='email-signin-button'
                                    onClick={ handleEmailSignIn }
                                    disabled={ componentDisabled }
                                >
                                    Send magic link
                                </Button>
                            </Box>
                        </Grid>

                        <Grid item xs={12}>
                            <Box sx={{ mb: 2 }}>
                                <Button
                                    variant="text" size="small" onClick={downloadLicenseAggreement}
                                >
                                    Terms of Service
                                </Button>
                            </Box>
                        </Grid>
                    </Grid>
                </ThemeProvider>
            </Container>
        </Box>
  );
}

export default Splash;
