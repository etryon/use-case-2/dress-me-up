/**
 * @fileOverview The Account page view
 */

import React, { useState, useEffect } from 'react';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import { makeStyles, styled } from '@mui/styles';
import PropTypes from 'prop-types';
import Typography from '@mui/material/Typography';
import logo from '../static/logo.png';
// import garmentsJson from '../static/all_garments.json';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import Select from '@mui/material/Select';
import { /* uploadGarmentsToFirebase, */ updateFirestoreUserProperty, firebaseChangeHeight } from '../helper';
import Box from '@mui/material/Box';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputAdornment from '@mui/material/InputAdornment';
// import Switch from '@mui/material/Switch';
import ScanComponent from '../components/ScanComponent';
import { BrowserView, MobileView } from 'react-device-detect';
import { useUserAuth } from '../contexts/AuthContext';
import { getFirestore, doc, getDoc, setDoc } from 'firebase/firestore';
import app from '../firebase/config';
import Container from '@mui/material/Container';
import Divider from '@mui/material/Divider';
import Chip from '@mui/material/Chip';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import ScanTutorial from '../components/ScanTutorial';
import Link from '@mui/material/Link';
/* require('dotenv').config(); */

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    '& > *': {
      margin: theme.spacing(1)
    }
  },
  margin: {
    margin: '0 auto',
    marginBottom: theme.spacing(4),
    marginTop: theme.spacing(4)
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7)
  },
  buttonStyle: {
    margin: '0 auto',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  formControl: {
    textAlign: 'left'
  },
  qrImageStyle: {
    width: '160px',
    height: '100%',
    padding: 0,
    position: 'static'
  }
}));

/**
 * Module for the Account view. Shows all the user information / settings.
 * @namespace Account
 * @returns {JSX.Element} The User account view.
 */
function Account () {
  const classes = useStyles();

  const { user, logOut } = useUserAuth();

  const [garmentGender, setGarmentGender] = useState('');
  const [size, setSize] = useState('');
  const [height, setHeight] = useState('');
  const [avatarGender, setAvatarGender] = useState('');
  const [avatarStatus, setAvatarStatus] = useState('');
  const [avatarDate, setAvatarDate] = useState('');
  const [lastScan, setLastScan] = useState('');
  const [showScanUI, setShowScanUI] = useState(false);
  /* const [demographicGender, setDemographicGender] = useState('');
  const [demographicAge, setDemographicAge] = useState('');
  const [trendsActive, setTrendsActive] = useState(false); */
  const db = getFirestore(app);

  const [dialogOpen, setDialogOpen] = React.useState(false);

  useEffect(async () => {
    if (Object.keys(user).length !== 0) {
      const userDataRef = doc(db, 'user_info', user.uid);
      if (garmentGender === '' && size === '') {
        const userDoc = await getDoc(userDataRef);
        if (userDoc.exists()) {
          setGarmentGender(userDoc.data().garment_gender);
          setSize(userDoc.data().size);
          setHeight(userDoc.data().height);
          setAvatarGender(userDoc.data().avatar_gender);
          setAvatarStatus(userDoc.data().avatar_status);
          /* setDemographicGender(userDoc.data().demographic_gender ? userDoc.data().demographic_gender : '');
          setDemographicAge(userDoc.data().demographic_age ? userDoc.data().demographic_age : '');
          setTrendsActive(userDoc.data().trends_active ? userDoc.data().trends_active : false); */

          if (userDoc.data().avatar_id) {
            const date = new Date(parseInt(userDoc.data().avatar_id));
            setAvatarDate(date.toLocaleString('en-GB'));
          } else {
            setAvatarDate('');
          }

          if (userDoc.data().last_scan) {
            const lastScanDate = new Date(parseInt(userDoc.data().last_scan));
            setLastScan(lastScanDate.toLocaleString('en-GB'));

            // Check if QC part has failed
            if (userDoc.data().avatar_status === 'pending') {
              const todayMil = Date.now();
              const todaysDate = new Date(todayMil);
              const mins = Math.abs(todaysDate - lastScanDate) / (60 * 1000);
              if (mins > 5) {
                setAvatarStatus('failed');
              }
            }
          } else {
            setLastScan('Please proceed with body scan');
          }
        } else {
          // Setting a dummy value to create Firestore entry
          await setDoc(userDataRef, { avatar_status: '' }, { merge: true });
        }
      }
    }
  }, [user]);

  /**
     * Handles the change of the Gender dropdown component and uploads newly selected Gender string to Firebase
     * @param {Event} event The dropdown selection event
     */
  const handleGarmentGenderChange = (event) => {
    if (event.target.value.length !== 0) {
      setGarmentGender(event.target.value);
      const userDataRef = doc(db, 'user_info', user.uid);
      updateFirestoreUserProperty(event, userDataRef, 'garment_gender');
    }
  };

  /**
     * Handles the change of the Avatar Gender dropdown component and uploads newly selected Avatar Gender string to Firebase
     * @param {Event} event The dropdown selection event
     */
  const handleAvatarGenderChange = (event) => {
    if (event.target.value.length !== 0) {
      setAvatarGender(event.target.value);
      const userDataRef = doc(db, 'user_info', user.uid);
      updateFirestoreUserProperty(event, userDataRef, 'avatar_gender');
    }
  };

  /**
     * Handles the change of the Size dropdown component and uploads newly selected Size string to Firebase
     * @param {Event} event The dropdown selection event
     */
  const handleSizeChange = (event) => {
    if (event.target.value.length !== 0) {
      setSize(event.target.value);
      const userDataRef = doc(db, 'user_info', user.uid);
      updateFirestoreUserProperty(event, userDataRef, 'size');
    }
  };

  /**
     * Handles the change of the Height input component
     * @param {Event} event The input value change event
     */
  const handleHeightChange = (event) => {
    if (event.target.value.length !== 0) {
      setHeight(event.target.value);
    }
  };

  const saveHeightChange = () => {
    if (height < 150) {
      setHeight('');
    } else if (height > 215) {
      setHeight('');
    } else {
      const userDataRef = doc(db, 'user_info', user.uid);
      firebaseChangeHeight(height, userDataRef);
    }
  };
    /**
     * Handles the change of the Demographic Gender dropdown component and uploads newly selected Demographic Gender string to Firebase
     * @param {Event} event The dropdown selection event
     */
  /* const handleDemographicGenderChange = (event) => {
    if (event.target.value.length !== 0) {
      setDemographicGender(event.target.value);
      const userDataRef = doc(db, 'user_info', user.uid);
      updateFirestoreUserProperty(event, userDataRef, 'demographic_gender');
    }
  }; */

  /**
     * Handles the change of the Demographic Age Range dropdown component and uploads newly selected Demographic Gender string to Firebase
     * @param {Event} event The dropdown selection event
     */
  /*  const handleDemographicAgeChange = (event) => {
    if (event.target.value.length !== 0) {
      setDemographicAge(event.target.value);
      const userDataRef = doc(db, 'user_info', user.uid);
      updateFirestoreUserProperty(event, userDataRef, 'demographic_age');
    }
  }; */

  /*  const handleDemographicSwitch = (event) => {
    if (event.target.value.length !== 0) {
      setTrendsActive(event.target.checked);
      const userDataRef = doc(db, 'user_info', user.uid);
      updateFirestoreUserProperty(event, userDataRef, 'trends_active');
    }
  }; */

  const handleClickOpen = () => {
    setDialogOpen(true);
  };

  const handleClose = () => {
    setDialogOpen(false);
  };

  const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
      padding: theme.spacing(2)
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1)
    }
  }));

  const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;

    return (
            <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
                {children}
                {onClose
                  ? (
                    <IconButton
                        aria-label="close"
                        onClick={onClose}
                        sx={{
                          position: 'absolute',
                          right: 8,
                          top: 8,
                          color: (theme) => theme.palette.grey[500]
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                    )
                  : null}
            </DialogTitle>
    );
  };

  BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired
  };

  // eslint-disable-next-line no-unused-vars
  /* const handleGarmentsUpload = () => {
          const garments = garmentsJson;
          uploadGarmentsToFirebase(garments);
        }; */

  return (
    showScanUI
      ? <React.StrictMode>
                <ScanComponent user={user} avatarGender={avatarGender.substring(0, 1).toUpperCase()} avatarHeight={height}/>
            </React.StrictMode>
      : <>
                <Container maxWidth="md" sx={{ width: '100%', overflowX: 'hidden', overflowY: 'auto' }}>

                    <BootstrapDialog
                        open={dialogOpen}
                        onClose={handleClose}
                        aria-labelledby="customized-dialog-title"
                    >
                        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                            Body Scanning Tips
                        </BootstrapDialogTitle>
                        <DialogContent dividers>
                            <ScanTutorial/>
                        </DialogContent>
                        <DialogActions /* style={{ justifyContent: 'space-between' }} */>
                            {/* <Button sx={{ color: '#777' }} onClick={ () => setShowScanUI(true) }>
                                Skip
                            </Button> */}
                            <Button onClick={ () => setShowScanUI(true) } autoFocus>
                                Proceed with Scan
                            </Button>
                        </DialogActions>
                    </BootstrapDialog>

                    <Grid container justifyContent="center" spacing={2} >
                        <Grid item xs={12}>
                            <div className="accountHeaderStyle">
                                <img src={logo} alt="Dress Me Up logo" />
                                <h1 className='OpenSansFont' style={{ textAlign: 'left' }}> DRESS ME UP </h1>
                            </div>

                                { user.email
                                  ? <Typography variant="subtitle2" display="block"> {user.displayName} ({user.email}) </Typography>
                                  : ''
                                }

                        </Grid>

                        <Grid item xs={12}>
                            <Button href="/" to="/" variant="outlined" color="secondary" size="large" className={classes.buttonStyle} onClick={() => {
                              logOut().catch((error) => {
                                console.log('error signing out: ', error);
                              });
                            }}>
                                SIGN OUT
                            </Button>
                        </Grid>

                        <Grid item xs={12} sm={6}>
                            <Box sx={{ mb: 2, mt: 2 }}>
                                <Divider>
                                    <Chip label="Garment" />
                                </Divider>
                                <Typography variant='caption'>The available garments will be filtered based on your selection</Typography>
                            </Box>
                            <FormControl error={!garmentGender} className={classes.formControl} required sx={{ m: 1 }}>
                                <InputLabel id="gender-select-helper-label">Style</InputLabel>
                                <Select
                                    labelId="gender-select-helper-label"
                                    id="gender-select-helper"
                                    value={garmentGender}
                                    label="Gender *"
                                    onChange={handleGarmentGenderChange}
                                >
                                    <MenuItem value="" disabled>
                                        <em>Select your Garment Style</em>
                                    </MenuItem>
                                    <MenuItem value={'female'}>Female</MenuItem>
                                    <MenuItem value={'male'}>Male</MenuItem>
                                </Select>
                                {(garmentGender) ? '' : <FormHelperText>Please set your Gender</FormHelperText> }
                            </FormControl>

                            <FormControl error={!size} className={classes.formControl} required sx={{ m: 1 }}>
                                <InputLabel id="size-helper-label">Size</InputLabel>
                                <Select
                                    labelId="size-select-helper-label"
                                    id="size-select-helper"
                                    value={size}
                                    label="Size *"
                                    onChange={handleSizeChange}
                                >
                                    <MenuItem value="" disabled>
                                        <em>Select your Size</em>
                                    </MenuItem>
                                    <MenuItem value={'XS'}>X-Small</MenuItem>
                                    <MenuItem value={'S'}>Small</MenuItem>
                                    <MenuItem value={'M'}>Medium</MenuItem>
                                    <MenuItem value={'L'}>Large</MenuItem>
                                    <MenuItem value={'XL'}>X-Large</MenuItem>
                                    <MenuItem value={'XXL'}>2X-Large</MenuItem>
                                </Select>
                                {(size) ? '' : <FormHelperText>Please set your preferred size</FormHelperText> }
                            </FormControl>

                            {/* <Box sx={{ mb: 2, mt: 2 }}>
                                <Divider>
                                    <Chip label="Target Demographic" />
                                </Divider>
                                <Typography variant='caption'>You can set a target demographic in order to get Trend Scores for the garments</Typography>
                            </Box>

                            <FormControl className={classes.formControl} sx={{ m: 1 }}>
                                <InputLabel id="demographic-gender-select-helper-label">Gender</InputLabel>
                                <Select
                                    labelId="demographic-gender-select-helper-label"
                                    id="demographic-gender-select-helper"
                                    value={demographicGender}
                                    label="Gender"
                                    onChange={handleDemographicGenderChange}
                                >
                                    <MenuItem value='' disabled>
                                        <em>Select your Demographic Gender</em>
                                    </MenuItem>
                                    <MenuItem value={'female'}>Female</MenuItem>
                                    <MenuItem value={'male'}>Male</MenuItem>
                                </Select>
                                {(demographicGender) ? '' : <FormHelperText>Please set your Demographic Gender</FormHelperText> }
                            </FormControl>

                            <FormControl className={classes.formControl} sx={{ m: 1 }}>
                                <InputLabel id="demographic-age-select-helper-label">Age Range</InputLabel>
                                <Select
                                    labelId="demographic-age-select-helper-label"
                                    id="demographic-age-select-helper"
                                    value={demographicAge}
                                    label="Age Range"
                                    onChange={ handleDemographicAgeChange }
                                >
                                    <MenuItem value='' disabled>
                                        <em>Select your Demographic Age Range</em>
                                    </MenuItem>
                                    <MenuItem value={'0-18'}>0 - 18</MenuItem>
                                    <MenuItem value={'18-25'}>18 - 25</MenuItem>
                                    <MenuItem value={'25-30'}>25 - 30</MenuItem>
                                    <MenuItem value={'30-35'}>30 - 35</MenuItem>
                                    <MenuItem value={'35-45'}>35 - 45</MenuItem>
                                    <MenuItem value={'45-55'}>45 - 55</MenuItem>
                                    <MenuItem value={'>55'}>&gt; 55</MenuItem>
                                </Select>
                                {(demographicAge) ? '' : <FormHelperText>Please set your Demographic Age Range</FormHelperText> }
                            </FormControl>

                            <FormControl className={classes.formControl} sx={{ m: 1 }}>
                                <FormControlLabel control={<Switch checked={ trendsActive } onChange={ (e) => handleDemographicSwitch(e)} />} label="Enable" />
                            </FormControl> */}

                        </Grid>

                        <Grid item xs={12} sm={6}>

                            <Box sx={{ mb: 2, mt: 2 }}>
                                <Divider>
                                    <Chip label="Avatar" />
                                </Divider>
                                <Typography variant='caption'>Create a 3D representation of your body</Typography>
                            </Box>

                            {avatarDate
                              ? <Alert sx={{ mb: 1 }} severity="success"><AlertTitle>Active Avatar Online</AlertTitle>You can now use the app! Created on: <b>{avatarDate}</b>.</Alert>
                              : <Alert sx={{ mb: 1 }} severity="warning"><b>No Active Avatar. Please add your information &amp; scan yourself if you haven&apos;t already using the button below.</b></Alert>
                            }
                            {(avatarStatus === 'failed')
                              ? <Alert sx={{ mb: 1 }} severity="error">Your last scan performed on: <b>{lastScan}</b> has <b>failed</b>, please try scanning your body again, making sure you follow our guide and have good lighting conditions.</Alert>
                              : ''
                            }
                            {(avatarStatus === 'pending' || avatarStatus === 'processing')
                              ? <Alert sx={{ mb: 1 }} severity="info">Last scan performed on: <b>{lastScan}</b> has status: <b>{avatarStatus}</b>. Please be patient while we process your data.</Alert>
                              : ''
                            }
                            <Box>
                                <FormControl error={!avatarGender} className={classes.formControl} required sx={{ m: 1 }}>
                                    <InputLabel id="avatar-gender-select-helper-label">Gender</InputLabel>
                                    <Select
                                        labelId="avatar-gender-select-helper-label"
                                        id="avatar-gender-select-helper"
                                        value={avatarGender}
                                        label="Gender *"
                                        onChange={handleAvatarGenderChange}
                                    >
                                        <MenuItem value="" disabled>
                                            <em>Select your Gender</em>
                                        </MenuItem>
                                        <MenuItem value={'female'}>Female</MenuItem>
                                        <MenuItem value={'male'}>Male</MenuItem>
                                    </Select>
                                    {(avatarGender) ? '' : <FormHelperText>Please set your Gender</FormHelperText> }
                                </FormControl>

                                <FormControl error={!height} sx={{ m: 1, textAlign: 'center' }} variant="outlined" required>
                                    <InputLabel htmlFor="outlined-adornment-height">Height</InputLabel>
                                    <OutlinedInput
                                        id="outlined-adornment-height"
                                        type="number"
                                        label="Height"
                                        endAdornment={<InputAdornment position="end">cm</InputAdornment>}
                                        aria-describedby="outlined-height-helper-text"
                                        onChange={handleHeightChange}
                                        onBlur={saveHeightChange}
                                        value={height}
                                        inputProps={{
                                          type: 'number',
                                          min: 150,
                                          max: 215,
                                          'aria-label': 'height'
                                        }}
                                    />
                                    <FormHelperText sx={{ textAlign: 'center' }} id="outlined-height-helper-text">Height must be between <b>150</b> & <b>215</b> cm!</FormHelperText>
                                </FormControl>
                            </Box>

                            <MobileView>
                                <Button disabled={!height || !avatarGender} variant="contained" color="secondary" size="large" className={classes.buttonStyle}
                                        onClick={handleClickOpen}
                                        fullWidth
                                        sx={{ mb: 1 }}
                                >
                                    Scan body
                                </Button>
                                <Typography variant="caption" sx={{ display: 'block' }}>
                                    Have a person hold your phone during the scanning process, and follow the instructions in order for your full body to be captured correctly!
                                </Typography>
                            </MobileView>

                            <BrowserView>
                                <Box
                                    component="img"
                                    className={classes.qrImageStyle}
                                    sx={{ objectFit: 'contain' }}
                                    src='qr-scan.png'
                                />
                                <Typography variant="caption" sx={{ display: 'block' }}>
                                    To scan your body reload this page by visiting again the <Link href="/account" to="/account">Dress Me Up</Link> web page
                                    through your mobile phone.
                                </Typography>
                                <Typography variant="caption" sx={{ display: 'block' }}>
                                You can use the QR code to quickly reload the page through your phones camera app.
                                </Typography>
                            </BrowserView>
                            {/* <Button variant="contained" color="primary" size="large" className={classes.buttonStyle}
                                onClick={() => {
                                  handleGarmentsUpload();
                                }}
                        >
                            Upload garments
                        </Button> */}

                        </Grid>
                    </Grid>
                </Container>
            </>
  );
}

export default Account;
