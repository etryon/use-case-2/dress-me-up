/**
 * @fileOverview The Collection page view
 */

import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@mui/styles';
import AppBar from '@mui/material/AppBar';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import ListItemText from '@mui/material/ListItemText';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import Divider from '@mui/material/Divider';
import Toolbar from '@mui/material/Toolbar';
import CloseIcon from '@mui/icons-material/Close';
import NoCollectionDataScreen from '../components/NoCollectionDataSplash';
import { getStorage, ref, getDownloadURL } from 'firebase/storage';
import Stack from '@mui/material/Stack';
import Chip from '@mui/material/Chip';
import { deleteCollectionItem, numDaysBetween } from '../helper';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import Slide from '@mui/material/Slide';
import TextSnippetIcon from '@mui/icons-material/TextSnippet';
import TabWithCount from '../components/TabWithCount';
import { useUserAuth } from '../contexts/AuthContext';
import app from '../firebase/config';
import Container from '@mui/material/Container';
import { getFirestore, collection, query, where, getDocs, doc, getDoc } from 'firebase/firestore';
import NoUserDataSplash from '../components/NoUserDataSplash';
import Tooltip from '@mui/material/Tooltip';
/**
 * Tab Panel Component
 * @memberOf Collection
 * @constructor
 * @returns {JSX.Element} The Tab Panel Component
 */
function TabPanel (props) {
  const { children, value, index, ...other } = props;

  return (
      <div
          role="tabpanel"
          hidden={value !== index}
          id={`full-width-tabpanel-${index}`}
          aria-labelledby={`full-width-tab-${index}`}
          {...other}
      >
        {value === index && (
            <Box>
              <Typography>{children}</Typography>
            </Box>
        )}
      </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

/**
 * Adds an aria-control value in each tab panel prop
 * @memberOf Collection
 * @param {Number} index - The index id of the tab panel
 * @returns {{"aria-controls": string, id: string}}
 */
function a11yProps (index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  },
  appBarTabs: {
  },
  tabPanel: {
    overflow: 'auto',
    overflowX: 'hidden',
    maxHeight: '82vh',
    height: 'auto'
  },
  title: {
    flexGrow: 1
  },
  previewOverlayImageStyle: {
    width: '100%',
    height: '100%',
    padding: 0,
    position: 'static'
  },
  overlayButtons: {
    position: 'absolute'
  },
  overlayBackBtn: {
    top: theme.spacing(2),
    left: theme.spacing(2)
  },
  overlayShareBtn: {
    top: theme.spacing(2),
    right: theme.spacing(12)
  },
  overlayDownloadBtn: {
    top: theme.spacing(2),
    right: theme.spacing(2)
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff'
  }
}));

/**
 * A component for showing the Collection View.
 * @namespace Collection
 * @returns {JSX.Element} The Collection view for managing user collections.
 */
function Collection () {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = useState(0);
  const [dataUri, setDataUri] = useState(null);
  const [readyList, setReadyList] = useState([]);
  const [pendingList, setPendingList] = useState([]);

  const [openDialog, setOpenDialog] = React.useState(false);
  const [focusedItem, setFocusedItem] = React.useState('false');
  const [imageDialogOpen, setImageDialogOpen] = React.useState(false);
  const [avatarId, setAvatarId] = React.useState(false);
  const user = useUserAuth().user;
  const db = getFirestore(app);
  const storage = getStorage();

  /**
   * Handles the click event on a Tab element for navigation.
   * @param {Event} event - The click event
   * @param {Number} newValue - The Tab element value
   */
  const handleChangeTab = (event, newValue) => {
    setValue(newValue);
  };

  /**
   * Handles the event when tab changes to show the correct content.
   * @param {Number} index - The Tab element index id
   */
  const handleChangeIndex = (index) => {
    setValue(index);
  };

  /**
   * Sets Image source url in order to view a Collection Item.
   * @param {URL} item - Link URL that points to the collection item for preview
   */
  const viewCollectionItem = (item) => {
    setDataUri(item);
    setImageDialogOpen(true);
  };

  /**
   * Creates a link in order for the user to download the Collection Item media file.
   * @param {URL} dataUri - The Collection Item media file url link
   */
  const downloadCollectionItem = (dataUri) => {
    const link = document.createElement('a');
    link.href = dataUri;
    link.setAttribute('download', 'dressMeUpExport.jpg'); // or any other extension
    document.body.appendChild(link);
    link.click();
  };

  const downloadLogs = (dataUri) => {
    const link = document.createElement('a');
    link.href = dataUri;
    link.setAttribute('download', 'logs.json'); // or any other extension
    document.body.appendChild(link);
    link.click();
  };

  const handleDialogOpen = (item) => {
    setFocusedItem(item);
    setOpenDialog(true);
  };

  const handleDialogClose = () => {
    setOpenDialog(false);
  };

  const handleImageDialogClose = () => {
    setImageDialogOpen(false);
    setDataUri(null);
  };

  const ImageDialogTransition = React.forwardRef(function Transition (props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

  useEffect(async () => {
    if (Object.keys(user).length !== 0) {
      const userDataRef = doc(db, 'user_info', user.uid);
      const userDoc = await getDoc(userDataRef);
      if (userDoc.exists()) {
        setAvatarId(userDoc.data().avatar_id);
      }
    }
  }, [user]);
  return (
      <>
        <Container maxWidth="md" sx={{ padding: 0, width: '100%', overflowX: 'hidden', overflowY: 'auto' }}>
        {
          /**
           * If dataUri is not empty, it means that a url has been loaded. Subsequently preview that media file.
           * Else the user is back in browsing all collection items.
           */
          (dataUri)
            ? <>
                <Dialog
                    fullScreen
                    open={imageDialogOpen}
                    onClose={handleImageDialogClose}
                    TransitionComponent={ImageDialogTransition}
                >
                  <AppBar sx={{ position: 'relative' }}>
                    <Toolbar>
                      <IconButton
                          edge="start"
                          color="inherit"
                          onClick={handleImageDialogClose}
                          aria-label="close"
                      >
                        <CloseIcon />
                      </IconButton>
                      <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                        {}
                      </Typography>
                      <Button autoFocus color="inherit" onClick={ () => downloadCollectionItem(dataUri) }>
                        Download
                      </Button>
                    </Toolbar>
                  </AppBar>

                  <Box
                      component="img"
                      className={classes.previewOverlayImageStyle}
                      sx={{
                        objectFit: 'contain'
                      }}
                      src={dataUri}
                  />

                </Dialog>
              </>
            : <>

                {
                (avatarId)
                  ? <>

                      <AppBar position="static">
                        <Toolbar>
                          <Typography variant="h6" className={classes.title}>
                            Collection Items
                          </Typography>
                        </Toolbar>
                      </AppBar>

                      <AppBar position="static" color='default' className={classes.root}>
                        <Tabs
                            value={value}
                            onChange={handleChangeTab}
                            indicatorColor="primary"
                            textColor="primary"
                            variant="fullWidth"
                        >
                          <Tab
                              label={<TabWithCount count={readyList.length}>Ready</TabWithCount>}
                              {...a11yProps(0)} className={classes.appBarTabs}
                          />
                          <Tab
                              label={<TabWithCount count={pendingList.length}>Pending</TabWithCount>}
                              {...a11yProps(1)} className={classes.appBarTabs}
                          />
                        </Tabs>
                      </AppBar>

                      <SwipeableViews
                          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                          index={value}
                          onChangeIndex={handleChangeIndex}
                      >

                        <TabPanel className={classes.tabPanel} value={value} index={0} dir={theme.direction}>

                          <CollectionListComponent status='ready'/>

                        </TabPanel>

                        <TabPanel className={classes.tabPanel} value={value} index={1} dir={theme.direction}>

                          <CollectionListComponent status='pending'/>

                        </TabPanel>
                      </SwipeableViews>
                    </>
                  : <>
                      <NoUserDataSplash/>
                </>
                }

              </>
        }
        </Container>
      </>
  );

  /**
   * Component that loads all media files in a list
   * @memberOf Collection
   * @param {Object} props - Object to denote in which view the user is in (Ready or Pending)
   * @returns {JSX.Element}
   */
  function CollectionListComponent (props) {
    useEffect(() => {
      if (Object.keys(user).length !== 0) {
        if (readyList.length === 0) {
          fetchItems('ready');
        }

        if (pendingList.length === 0) {
          fetchItems('pending');
        }
      }
    }, [user]);

    /**
     * It deletes a Ready collection item from Firebase.
     * @memberOf Collection
     * @param {Event} event - The event
     * @todo Change the created param to a unique id instead of the collection item created timestamp
     */
    const removeReadyItem = async (event) => {
      event.preventDefault();

      const uid = user.uid;

      await deleteCollectionItem('ready', focusedItem.created, db, uid);

      const newList = readyList.filter((item) => item.created !== focusedItem.created);

      setReadyList(newList);

      handleDialogClose();
    };

    /**
     * It deletes a Pending collection item from Firebase that has failed.
     * @memberOf Collection
     * @param {number} created - The created item timestamp
     * @param {Event} event - The event
     */
    const removePendingItem = async (created, event) => {
      event.preventDefault();

      const uid = user.uid;

      await deleteCollectionItem('pending', created, db, uid);

      const newList = pendingList.filter((item) => item.created !== created);
      setPendingList(newList);
    };

    /**
     * Fetches collection items from Firebase
     * @memberOf Collection
     * @param {String} status - The status of a collection item
     * @example status = 'pending'
     * @example status = 'ready'
     * @returns {Promise<void>} Returns
     */
    const fetchItems = async (status) => {
      try {
        const uid = user.uid;

        const q = query(collection(db, 'collection_items/users/' + uid + '/'), where('status', '==', status));

        const querySnapshot = await getDocs(q);
        if (querySnapshot.size > 0) {
          let allDocs = [];

          querySnapshot.forEach((doc) => {
            // doc.data() is never undefined for query doc snapshots

            const data = doc.data();
            const d = new Date(parseInt(data.created, 10));
            const localeOptions = { dateStyle: 'medium', timeStyle: 'short' };
            data.date = d.toLocaleString('en-GB', localeOptions);

            // Mark entry as failed to the UI if date.created > 3 days
            if (status === 'pending') {
              const todayMil = Date.now();
              const todaysDate = new Date(todayMil);
              if (numDaysBetween(todaysDate, d) > 3) {
                data.failed = true;
              }
            }
            allDocs.push(data);
          });
          allDocs = allDocs.sort((a, b) => b.created - a.created);

          /**
           * Update the corresponding state based on status.
           */
          if (status === 'pending') {
            for await (const doc of allDocs) {
              if (doc.failed) {
                const errorRef = ref(storage, 'collection/user/' + uid + '/output-error-' + doc.created + '.json');
                await getDownloadURL(errorRef)
                  .then((url) => {
                    doc.logsUrl = url;
                  });
              }
            }
            setPendingList(allDocs);
          } else {
            for (const doc of allDocs) {
              const storageRef = ref(storage, 'collection/user/' + uid + '/' + doc.output);
              getDownloadURL(storageRef).then((url) => {
                doc.url = url;
              })
                .catch((error) => {
                  console.log(error);
                });
            }
            setReadyList(allDocs);
          }
        }
      } catch (error) {
        console.log('error', error);
      }
    };

    return (
        <>
          <Dialog
              open={openDialog}
              onClose={handleDialogClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
          >
            <DialogTitle id="alert-dialog-title">
              {'Delete "' + focusedItem.name + '"?'}
            </DialogTitle>
            <DialogContent>
              <DialogContentText id="alert-dialog-description">
                If you delete this collection item you will not be able to get it back.
                Are you sure you want to delete it?
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleDialogClose}>Close</Button>
              <Button onClick={ (e) => removeReadyItem(e)} autoFocus>
                Delete
              </Button>
            </DialogActions>
          </Dialog>

          {
            (props.status === 'pending')
              ? <>
                  {
                    pendingList.length === 0
                      ? <NoCollectionDataScreen status='Pending' />
                      : <List sx={{ width: '100%', padding: '0' }}> {
                          pendingList.map((item) => (
                              <>
                                <ListItem key={item.created} >

                                  <ListItemText
                                      primary={item.name}
                                      secondary={item.date}
                                  />
                                  {
                                    item.failed
                                      ? <Stack direction="row" spacing={1}>
                                          <Chip
                                              label="Failed"
                                              onDelete={ (e) => removePendingItem(item.created, e) }
                                              deleteIcon={<Tooltip title="Delete"><DeleteIcon /></Tooltip>}
                                              color="error"
                                          />
                                        </Stack>
                                      : null
                                  }
                                  {
                                    item.logsUrl
                                      ? <IconButton edge="end" aria-label="logs" spacing={1}
                                                      onClick={ () => downloadLogs(item.logsUrl) }>
                                          <TextSnippetIcon/>
                                        </IconButton>
                                      : null
                                  }

                                </ListItem>
                                <Divider/>
                              </>
                          ))
                        }
                        </List>
                  }
                </>
              : <>
                  {
                    readyList.length === 0
                      ? <NoCollectionDataScreen status='Ready'/>
                      : <List > {
                          readyList.map((item) => (
                              <>
                                <ListItem key={item.created} media={item.url} button
                                          onClick={() => viewCollectionItem(item.url)}>
                                  <ListItemAvatar>
                                    <Avatar src={item.url} alt="Collection item"/>
                                  </ListItemAvatar>
                                  <ListItemText
                                      primary={item.name}
                                      secondary={item.date}
                                  />
                                  <ListItemSecondaryAction>

                                    <IconButton edge="end" aria-label="delete" spacing={1}
                                                onClick={() => handleDialogOpen(item)}>
                                      <DeleteIcon/>
                                    </IconButton>
                                  </ListItemSecondaryAction>

                                </ListItem>
                                <Divider/>
                              </>

                          ))
                        }
                        </List>
                  }
                </>
          }
        </>
    );
  }
}

export default Collection;
