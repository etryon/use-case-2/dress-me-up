import React from 'react';
import PropTypes from 'prop-types';

/**
 * Component for showing the image preview after the user has used the camera to capture a photo.
 * @module
 * @param {Object} dataUri Contains the photo details
 * @returns {JSX.Element} The image preview
 */
export const ImagePreview = ({ dataUri }) => {
  return (
        <div className={'camera-preview-fullscreen'}>
            <img alt={'captured from camera'} src={dataUri} />
        </div>
  );
};

ImagePreview.propTypes = {
  /**
     * The photo representation as a String.
     */
  dataUri: PropTypes.string,
  /**
     * Boolean denoting if the component will show in full screen.
     */
  isFullscreen: PropTypes.bool
};

export default ImagePreview;
