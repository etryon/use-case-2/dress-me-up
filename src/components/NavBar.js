import React, { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import RecentActorsIcon from '@mui/icons-material/RecentActors';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import Paper from '@mui/material/Paper';

/**
 * Returns the current url path the user is in.
 * @memberOf NavBar
 * @returns {String} location.pathname - url path.
 */
function ReturnLocationPath () {
  const location = useLocation();
  return location.pathname;
}

/**
 * The Navigation bar component.
 * @namespace NavBar
 * @returns {JSX.Element} The Navbar component.
 */
function NavBar () {
  const [value, setValue] = useState(0);

  const currentPage = ReturnLocationPath();

  useEffect(() => {
    if (currentPage === '/collection' && value !== 0) setValue(0);
    else if (currentPage === '/new' && value !== 1) setValue(1);
    else if (currentPage === '/account' && value !== 2) setValue(2);
  }, [value, currentPage]);

  return (

      <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0, zIndex: 2 }} elevation={3}>
        <BottomNavigation
            value={value}
            onChange={(event, newValue) => {
              setValue(newValue);
            }}
            showLabels
        >
            <BottomNavigationAction id='collection-menu-link' component={Link} to="/collection" label="Collection" icon={<RecentActorsIcon />} />
            <BottomNavigationAction id='new-menu-link' component={Link} to="/new" label="New" icon={<AddCircleOutlineIcon />} />
            <BottomNavigationAction id='account-menu-link' component={Link} to="/account" label="Account" icon={<AccountCircleIcon />} />

        </BottomNavigation>
      </Paper>
  );
}

export default NavBar;
