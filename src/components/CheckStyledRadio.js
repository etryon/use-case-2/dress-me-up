import React from 'react';
import Radio from '@mui/material/Radio';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';

const styles = () => ({
  root: {
    overflowX: 'hidden',
    '&:hover': {
      backgroundColor: 'transparent'
    }
  }
}
);
/**
 * A custom Radio Button that uses Google Material Design icons.
 */
class CheckStyleRadio extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      value: null
    };
  }

  /**
   * @returns {JSX.Element} The radio button component.
   */
  render () {
    return (
        <Radio
            className={styles.root}
            checkedIcon={<CheckCircleIcon color={'secondary'}/>}
            icon={<CheckCircleOutlineIcon/>}
            {...this.props}
        />
    );
  }
}
export default CheckStyleRadio;
