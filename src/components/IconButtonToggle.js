import React from 'react';
import ToggleIcon from "material-ui-toggle-icon";
import IconButton from "@mui/material/IconButton";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";

/**
 * A Toggle Button component that features an Icon instead of a classic button.
 */
class IconButtonToggle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: null,
        };
    }

    state = { on: false };
    /**
     * @returns {JSX.Element} The Icon Toggle button component.
     */
    render() {
        return (
            <IconButton onClick={() => this.setState((state) => ({ on: !state.on }))}>
                <ToggleIcon
                    on={this.state.on}
                    onIcon={<CheckCircleIcon />}
                    offIcon={<CheckCircleOutlineIcon />}
                />
            </IconButton>
        );
    }
}
export default IconButtonToggle;