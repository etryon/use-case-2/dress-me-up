import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import React from 'react';
import Link from '@mui/material/Link';
import logo from '../static/logo.png';
import { BrowserView, MobileView } from 'react-device-detect';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Chip from '@mui/material/Chip';
/**
 * Module that invokes a component for when the user hasn't set up the user data.
 *
 * @module
 * @returns {JSX.Element} The 'No User Data set up' view.
 */
function PleaseSetUserDataScreen () {
  return (
        <>
            <Grid item xs={12} style={{ textAlign: 'center' }}>
                <img src={logo} alt="Dress Me Up logo" className='App-logo'/>
                <Typography variant="h3" sx={{ fontFamily: "'Open Sans', sans-serif" }}> DRESS ME UP </Typography>
            </Grid>
            <Grid item xs={12}>
                <Box sx={{ mb: 2, mt: 2 }}>
                    <Divider>
                        <Chip label="Instructions" />
                    </Divider>
                </Box>
            </Grid>

            <BrowserView>
                <Grid container sx={{ pl: 2, pr: 2 }}>
                    <Typography color='textPrimary' variant="h5" gutterBottom textAlign='left'>
                        Please login again with your mobile phone to first scan your body!
                    </Typography>

                    <Grid item xs={4} >
                        <Box
                            component="img"
                            sx={{ objectFit: 'contain', width: '160px', height: '100%', padding: 0, position: 'static' }}
                            src='qr-scan.png'
                        />
                    </Grid>
                    <Grid item xs={8} textAlign='left'>
                        <Typography color='textSecondary' variant="h6" gutterBottom>Access your Account by visiting again the <Link href="/account" to="/account">Dress Me Up</Link> web page
                        through your mobile phone.</Typography>
                        <Typography color='textSecondary' variant="h6" gutterBottom>
                            Add your personal data &amp; scan your body.
                        </Typography>
                        <Typography color='textSecondary' variant="h6" gutterBottom>
                            Please wait for about 30-60 minutes for your avatar to be created.</Typography>
                        <Typography color='textSecondary' variant="h6" gutterBottom>
                            Once your Avatar is active, return to this page to start using the app.</Typography>
                    </Grid>
                    <Grid item xs={12} textAlign='left'>
                    <Typography color='textSecondary' variant="p" gutterBottom textAlign='left'>
                        You can check on the status of your <b>Active Avatar</b> in the <Link href="/account" to="/account">Account page</Link>.
                    </Typography>
                    </Grid>

                </Grid>
            </BrowserView>
            <MobileView>
                <Grid container sx={{ pl: 2, pr: 2 }}>
                    <Grid item xs={12} textAlign='center'>
                        <Typography color='textPrimary' variant="h5" gutterBottom>
                            Please go to
                        </Typography>

                        <Typography color='textPrimary' variant="h5" gutterBottom>
                            <Link href="/account" to="/account">
                                Account
                            </Link>
                        </Typography>
                        <Typography color='textPrimary' variant="h6" gutterBottom>
                            To scan your body and provide all needed information!
                        </Typography>

                        <Typography color='textSecondary' variant="h6" gutterBottom>
                            The avatar creation will take approximately 30 to 60 minutes.
                        </Typography>

                        <Typography color='textSecondary' variant="p" gutterBottom>
                            You can check on the status of your <b>Active Avatar</b> in the Account page.
                            In the unlikely event your scan fails, please try again!
                            Once your Avatar is active, come back to this page to use the app!
                        </Typography>
                    </Grid>
                </Grid>
            </MobileView>

        </>
  );
}

export default PleaseSetUserDataScreen;
