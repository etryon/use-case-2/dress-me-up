import React from 'react';
import { ApiSession, Scan } from '@quantacorp/react';
import { v4 as uuidv4 } from 'uuid';
import { Helmet } from 'react-helmet';
import { getFunctions, httpsCallable } from 'firebase/functions';
import { firebaseSetAvatar } from '../helper';
import Snackbar from '@mui/material/Snackbar';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import Button from '@mui/material/Button';
import { doc, getFirestore } from 'firebase/firestore';
import app, { analytics } from '../firebase/config';
import { logEvent } from 'firebase/analytics';

class ScanComponent extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      bodyId: 0,
      asyncScanId: 0,
      timestamp: null,
      loadingScanComponent: false,
      gender: this.props.avatarGender,
      height: (parseInt(this.props.avatarHeight, 10) * 10),
      minHeight: '100%',
      maxHeight: '100%',
      snackbarOpen: false,
      snackbarMessage: '',
      backdropOpen: true,
      user: this.props.user
    };

    this.setGender = this.setGender.bind(this);
    this.setHeight = this.setHeight.bind(this);
    this.clearDebugConsole = this.clearDebugConsole.bind(this);
    this.stopSpinner = this.stopSpinner.bind(this);
    this.debugEnabled = Boolean(parseInt(process.env.REACT_APP_QC_DEBUG, 10));
    this.setSnackbarOpen = this.setSnackbarOpen.bind(this);
    this.setBackdropOpen = this.setBackdropOpen.bind(this);
    this.exitComponent = this.exitComponent.bind(this);
    this.openErrorSnackbar = this.openErrorSnackbar.bind(this);

    // QC React SDK
    this.scanComponent = React.createRef();
    this.apiSession = new ApiSession(
      process.env.REACT_APP_QC_API_BASE,
      process.env.REACT_APP_QC_AUTH_CLIENT,
      process.env.REACT_APP_QC_AUTH_SECRET
    );
    this.initialiseScanComponent = this.initialiseScanComponent.bind(this);
    this.presentScanComponent = this.presentScanComponent.bind(this);
    this.didAppear = this.didAppear.bind(this);
    this.frontCaptured = this.frontCaptured.bind(this);
    this.sideCaptured = this.sideCaptured.bind(this);
    this.willDisappear = this.willDisappear.bind(this);
    this.cancel = this.cancel.bind(this);
    this.error = this.error.bind(this);
    this.authenticate = this.authenticate.bind(this);
    this.getAsyncScanStatus = this.getAsyncScanStatus.bind(this);
    this.startScanProcess = this.startScanProcess.bind(this);
  }

  componentDidMount () {
    const { width, height, availWidth, availHeight } = window.screen;
    this.debug('screen', { width, height, availWidth, availHeight });
    this.startScanProcess();
  }

  setGender (event) {
    this.setState({ gender: event.target.value }, () => this.debug('setGender', this.state.gender));
  }

  setHeight (event) {
    let { value, min, max } = event.target;
    value = parseInt(value);
    min = parseInt(min);
    max = parseInt(max);
    const heightValid = value >= min && value <= max;
    this.setState({ height: value }, () => this.debug('setHeight', `${this.state.height} [${heightValid ? 'valid' : 'invalid'}]`));
  }

  clearDebugConsole () {
    document.getElementById('debug-console').value = '';
  }

  stopSpinner () {
    this.setState({ loadingScanComponent: false });
  }

  debug (tag, message) {
    let debugMessage = message;
    const isObject = (typeof (message) === 'object');
    if (isObject) {
      debugMessage = JSON.stringify(message);
      if (debugMessage === '{}') {
        debugMessage = message.toString();
      }
    }
    const debugConsole = document.getElementById('debug-console');
    debugConsole.value = debugConsole.value + '\n\n' + tag + '\n' + debugMessage;
    debugConsole.scrollTop = debugConsole.scrollHeight;
    if (isObject) {
      console.debug(tag);
      console.debug(message);
    } else {
      console.debug(`${tag}: ${message}`);
    }
  }

  reset () {
    this.setState({
      loadingScanComponent: false
    });
  }

  getGenderFromInitial (genderInitial) {
    switch (genderInitial) {
      case 'F': return 'FEMALE';
      case 'M': return 'MALE';
      default: return 'UNKNOWN';
    }
  }

  getUniqueIdentifier () {
    return uuidv4();
  }

  setSnackbarOpen (event) {
    this.setState({ snackbarOpen: true });
  }

  setSnackbarMessage (msg) {
    this.setState({ snackbarMessage: msg });
  }

  setBackdropOpen (val) {
    this.setState({ backdropOpen: val });
  }

  exitComponent () {
    window.location.reload();
  }

  openErrorSnackbar (err) {
    this.setState({ snackbarMessage: err });
    this.setState({ snackbarOpen: true });
  }

  // QC React SDK
  async authenticate () {
    // 1. Authenticate

    const cloudFunctions = getFunctions(
      undefined,
      process.env.REACT_APP_CLOUD_FUNCTIONS_REGION
    );
    const tokenService = httpsCallable(cloudFunctions, 'token-service');
    let token;
    try {
      const result = await tokenService();
      const jwt = result.data.Token;
      try {
        if (jwt !== undefined) {
          token = await this.apiSession.authenticateWithJWT(jwt);
        }
      } catch (error) {
        this.setBackdropOpen(false);
        this.debug('authenticate', error);
        this.openErrorSnackbar('Authentication error.');
      }
    } catch (e) {
      console.log(`Error calling token-service: ${e}`);
      this.setBackdropOpen(false);
      this.openErrorSnackbar('Error calling token-service');
    }
    this.debug('authenticate', token);
  }

  async initialiseScanComponent () {
    try {
      // 2. Create a body
      const companyId = process.env.REACT_APP_QC_COMPANY_ID;
      const projectId = process.env.REACT_APP_QC_PROJECT_ID;
      const bodyDTO = {
        company_id: companyId,
        link_to_project: projectId,
        alias: this.getUniqueIdentifier(),
        height: this.state.height,
        gender: this.getGenderFromInitial(this.state.gender)
      };
      const bodyId = await this.apiSession.createBody(bodyDTO);
      this.debug('presentScanComponent - bodyId', bodyId);

      // 3. Register scan
      const asyncScanId = await this.apiSession.registerAsyncScan(projectId, bodyId);
      this.debug('presentScanComponent - asyncScanId', asyncScanId);

      this.setState({ bodyId: bodyId, asyncScanId: asyncScanId });

      // 4. Initialise scan component with gender and height
      // gender: ['U', 'M', 'F']
      // height: 1500 <= h <= 2150
      const initialised = this.scanComponent.current.initialise(this.state.gender, this.state.height);
      this.debug('initialiseScanComponent - initialised', initialised);
    } catch (error) {
      this.setBackdropOpen(false);
      this.debug('presentScanComponent', error);
      this.openErrorSnackbar('Initialization error');
    }
  }

  async presentScanComponent () {
    // 5. Present scan component
    this.setState({ loadingScanComponent: true });
    this.scanComponent.current.present();
    this.setBackdropOpen(false);
  }

  didAppear () {
    this.setState({ loadingScanComponent: false });
  }

  async frontCaptured (data) {
    // 6. Upload front data
    try {
      this.setBackdropOpen(true);
      this.debug('frontCaptured.metadata', data.metadata);
      const uploadResponse = await this.apiSession.uploadAsyncScanData(
        this.state.asyncScanId,
        'front',
        data
      );
      this.debug('frontCaptured', uploadResponse);
      this.setBackdropOpen(false);
    } catch (error) {
      this.setBackdropOpen(false);
      this.debug('frontCaptured', error);
      this.openErrorSnackbar('Front capture error. Please scan again!');
    }
  }

  async sideCaptured (data) {
    // 7. Upload side data
    try {
      this.setBackdropOpen(true);
      this.debug('sideCaptured.metadata', data.metadata);
      const uploadResponse = await this.apiSession.uploadAsyncScanData(
        this.state.asyncScanId,
        'side',
        data
      );
      this.debug('sideCaptured', uploadResponse);
      this.addAsyncScanCallbacks();
    } catch (error) {
      this.setBackdropOpen(false);
      this.debug('sideCaptured', error);
      this.openErrorSnackbar('Side capture error. Please scan again!');
    }
  }

  async addAsyncScanCallbacks () {
    // 8. Add callbacks

    const cloudFunctions = getFunctions(
      undefined,
      process.env.REACT_APP_CLOUD_FUNCTIONS_REGION
    );

    const urlMinter = httpsCallable(cloudFunctions, 'url-minter');

    const mintUrl = async (path) => {
      try {
        const result = await urlMinter([
          {
            Bucket: 'dress-me-up-pilot.appspot.com',
            Path: path,
            Method: 'PUT',
            TTL: '15m'
          }
        ]);
        // setMintedUrlData(result);
        return result.data[0].URL;
      } catch (e) {
        console.log(e);
        // setMintedUrlData(`Error calling url-minter: ${e}`);
        this.setBackdropOpen(false);
        this.openErrorSnackbar('Error calling url-minter');
      }
    };

    this.state.timestamp = Date.now();

    const plyUrl = await mintUrl('/qcscan/user/' + this.state.user.uid + '/scanatar-file-' + this.state.timestamp + '.ply');
    const metaUrl = await mintUrl('/qcmeta/user/' + this.state.user.uid + '/scanatar-meta-' + this.state.timestamp + '.json');

    try {
      const callbacks = {
        ply_star_callback: plyUrl,
        etryon_meta_callback: metaUrl
      };
      const callbacksResponse = await this.apiSession.addAsyncScanCallbacks(
        this.state.asyncScanId,
        callbacks
      );
      this.debug('addAsyncScanCallbacks', callbacksResponse);
      this.processAsyncScan();
    } catch (error) {
      this.setBackdropOpen(false);
      this.debug('addAsyncScanCallbacks', error);
      this.openErrorSnackbar('Error adding callbacks');
    }
  }

  async processAsyncScan () {
    // 9. Send process request for the scan
    try {
      const processResult = await this.apiSession.processAsyncScan(
        this.state.asyncScanId
      );
      this.debug('processAsyncScan', processResult);

      if (processResult.status === 202) {
        const db = getFirestore(app);
        const userDataRef = doc(db, 'user_info', this.state.user.uid);
        await firebaseSetAvatar(this.state.timestamp, 'pending', userDataRef);
        logEvent(analytics, 'avatar_upload',
          { user: this.state.user.uid });
        this.setBackdropOpen(false);
        this.setSnackbarMessage('Your scan has been submitted! In approximately 30 - 60 minutes your avatar will activate. You can then navigate to the "New" page to submit a new item!');
        this.setSnackbarOpen();
      }
    } catch (error) {
      this.setBackdropOpen(false);
      this.debug('processAsyncScan', error);
      this.openErrorSnackbar('Error sending request! Please try scanning again.');
    }
  }

  willDisappear () {
    this.debug('willDisappear', 'returning to implementing component');
  }

  async getAsyncScanStatus () {
    // 10. Call you can use to poll the status of the scan
    try {
      const statusResult = await this.apiSession.getAsyncScanStatus(
        this.state.asyncScanId
      );
      this.debug('getAsyncScanStatus', statusResult);
    } catch (error) {
      this.debug('getAsyncScanStatus', error);
    }
  }

  cancel () {
    this.debug('cancel', 'scanning was cancelled by user');
    window.location.reload();
  }

  error (message) {
    this.debug('error', message);
    this.reset();
  }

  async startScanProcess () {
    await this.authenticate();
    await this.initialiseScanComponent();
    await this.presentScanComponent();
  }

  render () {
    const snackbarAction = (
        <React.Fragment>
          <Button color="info" size="large" onClick={this.exitComponent}>
            Exit
          </Button>
        </React.Fragment>
    );

    return (

        <div style={{ position: 'relative', zIndex: 9999 }}>

          <Helmet>
            <meta
                name="viewport"
                content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
            />
          </Helmet>

          <Backdrop
              sx={{ color: '#fff', zIndex: 9999 }}
              open={this.state.backdropOpen}
          >
            <CircularProgress color="inherit" />
          </Backdrop>

          <Snackbar
              open={this.state.snackbarOpen}
              message={this.state.snackbarMessage}
              onClose={this.exitComponent}
              action={snackbarAction}
          >
          </Snackbar>

          <div className="content" hidden>
            <textarea id="debug-console" />
          </div>

          <Scan
              ref={this.scanComponent}
              zIndex={3000}
              onDidAppear={this.didAppear}
              onFrontCaptured={this.frontCaptured}
              onSideCaptured={this.sideCaptured}
              onWillDisappear={this.willDisappear}
              onCancel={this.cancel}
              onError={this.error}
              debug={this.debugEnabled}
              onDebug={this.debug}
          />
        </div>
    );
  }
}
export default ScanComponent;
