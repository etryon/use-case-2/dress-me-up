import Box from '@mui/material/Box';
import MobileStepper from '@mui/material/MobileStepper';
import Button from '@mui/material/Button';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import React from 'react';
import SwipeableViews from 'react-swipeable-views';
import { useTheme } from '@mui/styles';
import Typography from '@mui/material/Typography';
import step1Photo from '../static/scan_tutorial/step1.jpg';
import step2Photo from '../static/scan_tutorial/step2.jpg';
import step3Photo from '../static/scan_tutorial/step3.jpg';
import step4Photo from '../static/scan_tutorial/step4.jpg';
import step5Photo from '../static/scan_tutorial/step5.jpg';

function ScanTutorial () {
  const theme = useTheme();

  const scanTutorialItems = [
    {
      label: 'Grab a friend!',
      imgPath: step1Photo,
      description: ['Give your phone to a friend in order to scan you following our small guide.']
    },
    {
      label: 'Scanning preparation',
      imgPath: step2Photo,
      description: ['If you have long hair tie them up.',
        'Preferably wear tight clothing and flat shoes or socks.',
        'If your shirt is too long please tuck it in.',
        'Take off any accessories and remove anything from your pockets.']
    },
    {
      label: 'Take a photo from the front',
      imgPath: step3Photo,
      description: ['Stand with feet shoulder width apart.',
        'Spread your arms 45º apart.',
        'Stand straight, eyes looking straight ahead!',
        'Make sure your body fills the photo frame.'
      ]
    },
    {
      label: 'Take a photo from the side',
      imgPath: step4Photo,
      description: ['Rotate yourself 90º to your right.',
        'Stand with feet shoulder width apart.',
        'Arms pressed against your thighs.',
        'Stand straight, eyes looking straight ahead!',
        'Make sure your body fills the photo frame.']
    },
    {
      label: 'Done!',
      imgPath: step5Photo,
      description: ['Your 3D body is now processing!',
        'The process takes from 30 to 60 minutes.',
        'You can check on the status of your scan from the Account page.',
        'Sometimes the scan fails, but dont get disheartened, you can try again!']
    }
  ];

  const maxSteps = scanTutorialItems.length;
  const [activeStep, setActiveStep] = React.useState(0);

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
        <>
        <SwipeableViews
            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
            index={activeStep}
            onChangeIndex={handleStepChange}
            enableMouseEvents
        >
            {scanTutorialItems.map((step, index) => (
                <div key={step.label}>
                    <Typography sx={{ mb: 1, textAlign: 'center' }} variant='subtitle1'><b>{step.label}</b></Typography>
                    {Math.abs(activeStep - index) <= 2
                      ? (<>
                            <Box
                                component="img"
                                sx={{
                                  height: 'auto',
                                  display: 'block',
                                  overflow: 'hidden',
                                  width: '100%',
                                  mb: 1
                                }}
                                src={step.imgPath}
                                alt={step.label}
                            />
                                { step.description.map((item) => (
                                    <>
                                    <Typography sx={{ pb: 0.5, lineHeight: 1.5 }} key={item} variant='subtitle2'>{item}</Typography>
                                    </>
                                ))}

                            </>
                        )
                      : null}

                </div>
            ))}
        </SwipeableViews>
    <MobileStepper
        steps={maxSteps}
        position="static"
        activeStep={activeStep}
        nextButton={
            <Button
                size="small"
                onClick={handleNext}
                disabled={activeStep === maxSteps - 1}
            >
                Next
                {theme.direction === 'rtl'
                  ? (
                        <KeyboardArrowLeft />
                    )
                  : (
                        <KeyboardArrowRight />
                    )}
            </Button>
        }
        backButton={
            <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
                {theme.direction === 'rtl'
                  ? (
                        <KeyboardArrowRight />
                    )
                  : (
                        <KeyboardArrowLeft />
                    )}
                Back
            </Button>
        }
    />
        </>
  );
}

export default ScanTutorial;
