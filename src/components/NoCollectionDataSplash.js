import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import React from 'react';
import Link from '@mui/material/Link';
import logo from '../static/logo.png';

/**
 * This module creates a component - An empty view to show when no Collection data are available.
 *
 * @module
 * @param {Object} props Contains a string that denotes in which state of the Collection view the user is in (Ready or Pending).
 * @example props.status = 'Ready'
 * @example props.status = 'Pending'
 * @returns {JSX.Element} The 'No collection data' view
 */
function NoCollectionDataScreen (props) {
  return (
        <Grid container style={{ minHeight: '70vh' }} align="center" justifyContent="center" alignItems="center">
            <Grid item xs={12} style={{ textAlign: 'center' }}>

                <img src={logo} alt="Dress Me Up logo" className='App-logo'/>
                <Typography variant="h3" sx={{ fontFamily: "'Open Sans', sans-serif" }}> DRESS ME UP </Typography>
                <Typography color='textSecondary' variant="h6" gutterBottom>
                    No {props.status} Collection data available.
                </Typography>
                {
                    props.status === 'Ready'
                      ? <Typography color='textSecondary' variant="h6" gutterBottom>
                           Please wait for a new collection item to be processed
                            or go to
                            <Link href="/new" to="/new"> New </Link>
                            and upload a new collection item.
                        </Typography>
                      : <Typography color='textSecondary' variant="h6" gutterBottom>
                                Please go to&nbsp;
                                <Link href="/new" to="/new">
                                    New
                                </Link> and upload a new collection item first!
                            </Typography>

                }
            </Grid>
        </Grid>

  );
}

export default NoCollectionDataScreen;
