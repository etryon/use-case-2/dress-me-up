import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';

const TabWithCount = ({ children, count }) => {
  return (
        <Box sx={{ display: 'inline-flex', alignItems: 'center' }}>
            <Typography component="div">{children}</Typography>
            {count
              ? (
                <Chip
                    sx={{ marginLeft: '0.5rem' }}
                    label={count}/>
                )
              : null}
        </Box>
  );
};

export default TabWithCount;
