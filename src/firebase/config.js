import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getAnalytics } from 'firebase/analytics';

const firebaseConfig = {
  apiKey: 'AIzaSyAbOUxEqpoqkd7rYovNEIFPK0qEtLwwXSE',
  authDomain: 'dress-me-up-pilot.firebaseapp.com',
  projectId: 'dress-me-up-pilot',
  storageBucket: 'dress-me-up-pilot.appspot.com',
  messagingSenderId: '79604152726',
  appId: '1:79604152726:web:b3ce06ae73d16ae4d885b7',
  measurementId: 'G-7T2HMV02YB'
};

const app = initializeApp(firebaseConfig);
export const analytics = getAnalytics(app);
export const auth = getAuth(app);
export default app;
