import React, { createContext, useContext, useEffect, useState } from 'react';
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
  GoogleAuthProvider,
  signInWithPopup,
  sendSignInLinkToEmail,
  isSignInWithEmailLink,
  signInWithEmailLink
} from 'firebase/auth';

import { auth } from '../firebase/config';

const userAuthContext = createContext();

export function UserAuthContextProvider ({ children }) {
  const [user, setUser] = useState({});

  function logIn (email, password) {
    return signInWithEmailAndPassword(auth, email, password);
  }
  function signUp (email, password) {
    return createUserWithEmailAndPassword(auth, email, password);
  }
  function logOut () {
    return signOut(auth);
  }
  function googleSignIn () {
    const googleAuthProvider = new GoogleAuthProvider();
    return signInWithPopup(auth, googleAuthProvider);
  }

  function sendSignInEmail (email) {
    const actionCodeSettings = {
      url: 'https://dress-me-up-pilot.web.app',
      // This must be true.
      handleCodeInApp: true
    };
    return sendSignInLinkToEmail(auth, email, actionCodeSettings);
  }
  function authenticateWithEmail () {
    if (isSignInWithEmailLink(auth, window.location.href)) {
      let email = window.localStorage.getItem('emailForSignIn');
      if (!email) {
        // User opened the link on a different device. To prevent session fixation
        // attacks, ask the user to provide the associated email again. For example:
        email = window.prompt('Please provide your email for confirmation');
      }
      // The client SDK will parse the code from the link for you.
      signInWithEmailLink(auth, email, window.location.href)
        .then((result) => {
          // Clear email from storage.
          window.localStorage.removeItem('emailForSignIn');
          // You can access the new user via result.user
          // Additional user info profile not available via:
          // result.additionalUserInfo.profile == null
          // You can check if the user is new or existing:
          // result.additionalUserInfo.isNewUser
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (currentuser) => {
      setUser(currentuser);
    });

    return () => {
      unsubscribe();
    };
  }, []);

  return (
      <userAuthContext.Provider
          value={{ user, logIn, signUp, logOut, googleSignIn, sendSignInEmail, authenticateWithEmail }}
      >
        {children}
      </userAuthContext.Provider>
  );
}

export function useUserAuth () {
  return useContext(userAuthContext);
}
